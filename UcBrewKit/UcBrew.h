//
//  UcBrew.h
//  UcBrewKit
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UcBrew : NSObject <NSObject>
+ (NSString*) userPath;                                                                                                          
+ (NSString*) documentPath;                                                                                                      
+ (NSString*) libraryPath;                                                                                                       
+ (NSString*) downloadsPath;                                                                                                     
+ (NSString*) cachesPath;                                                                                                        
+ (NSString*) appPath;
+ (NSString*) bundlePath;
@end

BOOL UcBrew_IsNotEmptyString (id object);