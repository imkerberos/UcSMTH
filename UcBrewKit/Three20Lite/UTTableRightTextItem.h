//
//  UTTableRightTextItem.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTTableTextItem.h"

@interface UTTableRightTextItem : TTTableTextItem
+ (id) itemWithText:(NSString *)text;
+ (id) itemWithText:(NSString *)text accessory:(UITableViewCellAccessoryType)accessoryType;
+ (id) itemWithText:(NSString *)text delegate:(id)delegate selector:(SEL)selector;
@end
