//
//  UTURLHtmlResponse.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTURLResponse.h"
@class DDXMLDocument;

@interface UTURLHtmlResponse : NSObject <TTURLResponse>
@property (nonatomic, retain, readonly) DDXMLDocument* htmlDocument;
@end
