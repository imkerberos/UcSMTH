//
//  UTTableLongSubtitleItemCell.m
//  UcBrewKit
//
//  Created by Kerberos Zhang on 12-6-15.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UTTableLongSubtitleItem.h"
#import "UTTableLongSubtitleItemCell.h"

static const CGFloat kMaxLabelHeight = 2000.0f;
static const UILineBreakMode kLineBreakMode = UILineBreakModeCharacterWrap;
static const CGFloat kTableCellHackHMargin = 2.0f;

@implementation UTTableLongSubtitleItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.font = TTSTYLEVAR(tableFont);
        self.textLabel.textColor = TTSTYLEVAR(textColor);
        self.textLabel.highlightedTextColor = TTSTYLEVAR(highlightedTextColor);
        self.textLabel.backgroundColor = TTSTYLEVAR(backgroundTextColor);
        self.textLabel.textAlignment = UITextAlignmentLeft;
        self.textLabel.lineBreakMode = kLineBreakMode;
        self.textLabel.adjustsFontSizeToFitWidth = NO;
        self.textLabel.numberOfLines = 2;
        self.detailTextLabel.font = TTSTYLEVAR(font);
        self.detailTextLabel.textColor = TTSTYLEVAR(tableSubTextColor);
        self.detailTextLabel.highlightedTextColor = TTSTYLEVAR(highlightedTextColor);
        self.detailTextLabel.backgroundColor = TTSTYLEVAR(backgroundTextColor);
        self.detailTextLabel.textAlignment = UITextAlignmentLeft;
        self.detailTextLabel.contentMode = UIViewContentModeTop;
        self.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        self.detailTextLabel.numberOfLines = kTableMessageTextLineCount;
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+ (CGFloat)tableView:(UITableView *)tableView rowHeightForObject:(id)object
{
    UTTableLongSubtitleItem* item = object;
    CGFloat width = tableView.width - (kTableCellHPadding * 2 + [tableView tableCellMargin] * 2) - kTableCellSpacing;
    if (item.accessoryType != UITableViewCellAccessoryNone) {
        width -= kTableCellSpacing;
    }
    UIFont* font = TTSTYLEVAR(tableFont);
    CGSize size = [item.text sizeWithFont:font
                   constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)
                            lineBreakMode:kLineBreakMode];
    
    NSInteger textLines = item.maxTextLine ? item.maxTextLine : 1;
    NSInteger maxTextHeight = TTSTYLEVAR (tableFont).ttLineHeight * textLines;
    if (size.height > maxTextHeight) {
        size.height = maxTextHeight;
    }
    
    if (item.subtitle) {
        size.height += TTSTYLEVAR(font).ttLineHeight;
    }
    return size.height + kTableCellHackHMargin * 2;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    /*
     * TODO relayout
     */
    CGFloat width = self.contentView.width - kTableCellSpacing*2;
    if (self.accessoryType != UITableViewCellAccessoryNone) {
        //width -= (kTableCellMargin);
    }
    
    CGFloat left = kTableCellHPadding;
    CGSize size = [self.textLabel.text sizeWithFont:TTSTYLEVAR(tableFont)
                    constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)
                        lineBreakMode:kLineBreakMode];
    
    NSInteger textLines = self.textLabel.numberOfLines ? self.textLabel.numberOfLines : 1;
    NSInteger maxTextHeight = TTSTYLEVAR (tableFont).ttLineHeight * textLines;
    if (size.height > maxTextHeight) {
        size.height = maxTextHeight;
    }
    
    CGFloat textHeight = size.height;
    CGFloat height = textHeight + kTableCellHackHMargin * 2;
    
    if (self.detailTextLabel.text.length) {
        height = textHeight + TTSTYLEVAR(font).ttLineHeight;
        CGFloat subtitleHeight = TTSTYLEVAR(font).ttLineHeight;
        self.textLabel.frame = CGRectMake(left, kTableCellHackHMargin, width, textHeight);
        self.detailTextLabel.frame = CGRectMake(left, self.textLabel.bottom, width, subtitleHeight);
    } else {
        self.textLabel.frame = CGRectMake(left, kTableCellHackHMargin, width, textHeight);
        self.detailTextLabel.frame = CGRectZero;
    }
}

- (void)setObject:(id)object
{
    if (_item != object) {
        [super setObject:object];
        UTTableLongSubtitleItem* item = object;
        
        if (item.text.length) {
            self.textLabel.text = item.text;
        }
        if (item.maxTextLine) {
            self.textLabel.numberOfLines = item.maxTextLine;
        }
        if (item.subtitle.length) {
            self.detailTextLabel.text = item.subtitle;
        }
    }
}

- (UILabel*)subtitleLabel
{
    return self.detailTextLabel;
}
@end