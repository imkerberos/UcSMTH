//
//  TTTableImageItem+UcBrew.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTTableImageItem+UcBrew.h"

@implementation TTTableImageItem (UcBrew)

+ (id)itemWithText:(NSString*)text
          imageURL:(NSString*)imageURL
         accessory: (UITableViewCellAccessoryType) accessoryType
{
    TTTableImageItem* item = [TTTableImageItem itemWithText:text
                                                   imageURL:imageURL];
    item.accessoryType = accessoryType;
    return item;
}

@end
