//
//  UTTableLongSubtitleItem.m
//  UcBrewKit
//
//  Created by Kerberos Zhang on 12-6-15.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UTTableLongSubtitleItem.h"
#import "UTTableLongSubtitleItemCell.h"

const NSInteger kTextMaxLines = 2;

@implementation UTTableLongSubtitleItem
@synthesize subtitle;
@synthesize maxTextLine;

- (void)dealloc
{
    UCBREW_RELEASE(subtitle);
    [super dealloc];
}

- (Class)cellClass
{
    return [UTTableLongSubtitleItemCell class];
}

+ (id)itemWithText: (NSString*)text
          subtitle:(NSString*)subtitle
{
    UTTableLongSubtitleItem* item = [[[self alloc] init] autorelease];
    item.text = text;
    item.subtitle = subtitle;
    item.maxTextLine = kTextMaxLines;
    return item;
}

+ (id)itemWithText: (NSString*)text
          subtitle:(NSString*)subtitle
       maxTextLine: (NSInteger) textLine
{
    UTTableLongSubtitleItem* item = [[[self alloc] init] autorelease];
    item.text = text;
    item.subtitle = subtitle;
    item.maxTextLine = textLine;
    return item;
}

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
         accessory: (UITableViewCellAccessoryType)accessoryType
{
    UTTableLongSubtitleItem* item = [[[self alloc] init] autorelease];
    item.text = text;
    item.subtitle = subtitle;
    item.maxTextLine = kTextMaxLines;
    item.accessoryType = accessoryType;
    return item;
}

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
       maxTextLine: (NSInteger) textLine
         accessory: (UITableViewCellAccessoryType)accessoryType
{
    UTTableLongSubtitleItem* item = [[[self alloc] init] autorelease];
    item.text = text;
    item.subtitle = subtitle;
    item.accessoryType = accessoryType;
    item.maxTextLine = textLine;
    return item;
}

+ (id)itemWithText:(NSString*)text
          subtitle:(NSString*)subtitle
               URL:(NSString*)URL
{
    UTTableLongSubtitleItem* item = [[[self alloc] init] autorelease];
    item.text = text;
    item.subtitle = subtitle;
    item.maxTextLine = kTextMaxLines;
    return item;
}


+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
               URL: (NSString*)URL
      accessoryURL: (NSString*)accessoryURL
{
    UTTableLongSubtitleItem* item = [[[self alloc] init] autorelease];
    item.text = text;
    item.subtitle = subtitle;
    item.maxTextLine = kTextMaxLines;
    return item;
}

- (id)initWithCoder:(NSCoder*)decoder {
	self = [super initWithCoder:decoder];
    if (self) {
        self.subtitle = [decoder decodeObjectForKey:@"subtitle"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder*)encoder
{
    [super encodeWithCoder:encoder];
    if (self.subtitle) {
        [encoder encodeObject:self.subtitle forKey:@"subtitle"];
    }
}

@end