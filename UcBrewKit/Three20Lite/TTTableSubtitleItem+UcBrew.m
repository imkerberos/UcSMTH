//
//  TTTableSubtitleItem+UcBrew.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTTableSubtitleItem+UcBrew.h"

@implementation TTTableSubtitleItem (UcBrew)

+ (id)itemWithText:(NSString *)text
          subtitle:(NSString *)subtitle
         accessory: (UITableViewCellAccessoryType) accessoryType
{
    TTTableSubtitleItem* item = [TTTableSubtitleItem itemWithText:text
                                                         subtitle:subtitle];
    item.accessoryType = accessoryType;
    return item;
} 

@end
