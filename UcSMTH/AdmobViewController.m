//
//  AdmobControllerViewController.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 13-5-8.
//  Copyright (c) 2013年 UcBrew. All rights reserved.
//

#import "UcSMTHConfig.h"
#import "AdmobViewController.h"
#import "GADAdSize.h"

@interface AdmobViewController ()

@end

@implementation AdmobViewController
{
    GADBannerView* _admobView;
}

@synthesize admobView = _admobView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _admobView = [[GADBannerView alloc] initWithFrame:CGRectZero];
    _admobView.adUnitID = @"a14f55c6b3e9cb5";
    _admobView.delegate = self;
    _admobView.adSize = kGADAdSizeSmartBannerPortrait;
    [_admobView setRootViewController: self];
    [self.view addSubview: _admobView];
    [self performSelector: @selector(requestAd) withObject: nil afterDelay: 3.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [_admobView release], _admobView = nil;
    [super dealloc];
}


- (void) requestAd
{
    GADRequest *request = [GADRequest request];
    request.testDevices = [NSArray arrayWithObjects:@"GAD_SIMULATOR_ID", nil];
    [_admobView loadRequest: request];
}

- (void)layoutGADBannerView {
    CGSize bannerCGSize = CGSizeFromGADAdSize(_admobView.adSize);
    CGRect frame = _admobView.frame;
    CGSize screenSize = CGSizeMake(self.view.bounds.size.width,
                                   self.view.bounds.size.height);
    frame.origin.x = (screenSize.width - bannerCGSize.width) / 2.0;
    frame.origin.y = screenSize.height - bannerCGSize.height;
    _admobView.frame = frame;
    if (_admobView.hidden) {
        _admobView.hidden = NO;
    }
    UIView* navView = [self.view viewWithTag: UCSMTH_NAVIGATOR_VIEW_TAG];
    if (navView) {
        frame = navView.frame;
        frame.size.height = screenSize.height - _admobView.frame.size.height;
        navView.frame = frame;
    }
}

- (void) adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"admob recieved ad!\n");
    [self layoutGADBannerView];
}

- (void) adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Failed to receive banner ad: %@", [error debugDescription]);
}
@end
