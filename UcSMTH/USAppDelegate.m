//
//  USAppDelegate.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "iVersion.h"
#import "UcSMTHEngine.h"
#import "USAppDelegate.h"
#import "USNavigationController.h"
#import "USRootVC.h"
#import "AdmobViewController.h"
#import "WrapController.h"

#import "USBoardFeedParser.h"
#import "TestParsetKit.h"

@implementation USAppDelegate
{
@private
    UIViewController* _navController;
}


@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize navController = _navController;

+ (void) initialize
{
    [iVersion sharedInstance];
}
- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    AdmobViewController* view_controller = [[AdmobViewController alloc] init];
    view_controller.view.frame = CGRectMake (0.0f, 0.0f, 320.f, 480.f);
    self.viewController = view_controller;
    [view_controller release];
    USNavigationController* navController = [[[USNavigationController alloc]  initWithRootViewController:[USRootVC viewController]]
                           autorelease];
    navController.view.frame = CGRectMake (0.0f, 0.0f, 320.f, 480.f);
    [self.viewController.view addSubview: navController.view];
    navController.view.tag = UCSMTH_NAVIGATOR_VIEW_TAG;
    self.navController = navController;
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];

    [[UcSMTHEngine sharedEngine] setupEngine];
    [[UcSMTHEngine sharedEngine] setupDefaultConfig];
    [[UcSMTHEngine sharedEngine] isLogin];
//////////////////////////////////////////////////////////////////////////////// 
    //////////////////////////////////////////////////////////////////////////////// 
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
