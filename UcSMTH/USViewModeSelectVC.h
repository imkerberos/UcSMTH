//
//  USViewModeSelectVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-7-3.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#define CK_SHORTHAND
#import "MwfTableViewController.h"
@class USViewModeSelectVC;

@protocol USViewModeSelectVCDelegate <NSObject>
@optional
- (void) viewModeSelectDidFinish: (USViewModeSelectVC*) viewController mode: (NSInteger) mode;
- (void) viewModeSelectDidCancel: (USViewModeSelectVC*) viewController;
@end

@interface USViewModeSelectVC : MwfTableViewController
@property (nonatomic, assign) id<USViewModeSelectVCDelegate> delegate;

+ (id) viewControllerWithDelegate: (id<USViewModeSelectVCDelegate>) delegate;
+ (id) viewControllerWithMode: (NSInteger) aMode delegate: (id<USViewModeSelectVCDelegate>) delegate;
@end
