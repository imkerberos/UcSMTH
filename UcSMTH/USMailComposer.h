//
// Created by kerberos on 12-8-27.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "NFMessageComposerVC.h"

typedef enum {
    USMailComposerModeNone = 0,
    USMailComposerModeNew = 1,
    USMailComposerModeReply = 2,
} USMailComposerMode;

#define kUSMailComposerInbox @"inbox"
#define kUSMailComposerOutbox @"outbox"
#define kUSMailComposerDeletebox @"deleted"
#define kUSMailComopserNullbox  @"NULL"

@interface USMailComposer : NFMessageComposerVC
@property (nonatomic, copy) NSString* boxId;
@property (nonatomic, copy) NSString* mailId;

+ (id) composerWithMode: (USMailComposerMode) mode;
@end