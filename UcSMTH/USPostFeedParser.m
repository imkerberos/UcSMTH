//
//  USPostFeedParser.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "ParseKit.h"
#import "UcBrewKit.h"
#import "UcSMTHConfig.h"
#import "USPostFeedParser.h"
////////////////////////////////////////////////////////////////////////////////
@implementation USPostFeedParserItem
@synthesize postId;
@synthesize postAuthor;
@synthesize totalPage;
@end

////////////////////////////////////////////////////////////////////////////////

static NSString* board_page_grammer = @""
        @"@wordChars = '.';"
        @"@start = boardPageHref;"
        @"boardPageHref = '/' 'board' '/' boardId ('/' Number)* '?' 'p' '=' boardPages;"
        @"boardId = Word;"
        @"boardPages = Number;"
        @"";                  

static NSString* board_feedid_grammer = @""
        @"@wordChars = '.';"
        @"@start = feedHref;"
        @"feedHref = '/' 'article' '/' boardId ('/' 'single')? '/' feedId ('/' Number)?;"
        @"boardId = (Word | Number)+;"
        @"feedId = Number;"
        @"";

static NSString* board_feedauthor_grammer = @""
        @"@wordChars = '.';"
        @"@start = authorHref;"
        @"authorHref = '/' 'user' '/' 'query' '/' feedAuthorId;"
        @"feedAuthorId = (Word | Number)+;"
        @"";

@interface USPostFeedAssembly : NSObject
- (void) parser: (PKParser*) parser didMatchBoardPages: (PKAssembly*) assembly;
- (void) parser: (PKParser*) parser didMatchFeedId: (PKAssembly*) assembly;
- (void) parser: (PKParser*) parser didMatchFeedAuthorId: (PKAssembly*) assembly;
@end

@implementation USPostFeedAssembly
- (void) parser: (PKParser*) parser didMatchBoardPages: (PKAssembly*) assembly
{
    NSNumber* result = (NSNumber*) assembly.target;
    if (result != nil){
    } else {
        result = [NSNumber numberWithInt: (NSInteger)[[assembly pop] floatValue]];
        assembly.target = result;
    }
}

- (void) parser: (PKParser*) parser didMatchFeedId: (PKAssembly*) assembly
{
    NSString* result = (NSString*) assembly.target;
    if (UcBrew_IsNotEmptyString(result)){
    } else {
        assembly.target = [[assembly pop] stringValue];
    }
    
}

- (void) parser: (PKParser*) parser didMatchFeedAuthorId: (PKAssembly*) assembly
{
    NSString* result = (NSString*) assembly.target;
    if (UcBrew_IsNotEmptyString(result)){
    } else {
        assembly.target = [[assembly pop] stringValue];
    }
}

- (void) parser:(PKParser *)parser didMatchBoardId:(PKAssembly *)assembly
{
}
@end

////////////////////////////////////////////////////////////////////////////////
static USPostFeedParser* sharedParser = nil;

@interface USPostFeedParser ()
@property (nonatomic, retain) USPostFeedAssembly* assembly;
@property (nonatomic, retain) PKParser* boardPageParser;
@property (nonatomic, retain) PKParser* feedIdParser;
@property (nonatomic, retain) PKParser* feedAuthorIdParser;
@end

@implementation USPostFeedParser
@synthesize assembly;
@synthesize boardPageParser;
@synthesize feedIdParser;
@synthesize feedAuthorIdParser;

+ (id) sharedParser
{
    @synchronized ([USPostFeedParser class]) {
        if (sharedParser == nil) {
            sharedParser = [[USPostFeedParser alloc] init];
        }
    }
    
    return sharedParser;
}

- (id) init
{
    @synchronized ([USPostFeedParser class]) {
        if (self = [super init]) {
            self.assembly = [[[USPostFeedAssembly alloc] init] autorelease];
            self.boardPageParser = [[PKParserFactory factory] parserFromGrammar: board_page_grammer assembler: self.assembly];
            self.feedIdParser = [[PKParserFactory factory] parserFromGrammar: board_feedid_grammer assembler: self.assembly];
            self.feedAuthorIdParser = [[PKParserFactory factory] parserFromGrammar: board_feedauthor_grammer assembler: self.assembly];
        }
    }
    
    return self;
}

+ (id) allocWithZone:(NSZone *)zone
{
    @synchronized ([USPostFeedParser class]) {
        if (sharedParser == nil) {
            sharedParser = [super allocWithZone: zone];
        }
    }
    
    return sharedParser;
}

- (void) dealloc
{
    UCBREW_ASSERT (NO);
    PKReleaseSubparserTree (boardPageParser);
    UCBREW_RELEASE (boardPageParser);
    PKReleaseSubparserTree (feedIdParser);
    UCBREW_RELEASE (feedIdParser)
    PKReleaseSubparserTree (feedAuthorIdParser);
    UCBREW_RELEASE (feedAuthorIdParser)
    [super dealloc];
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (oneway void) release
{
}

- (id) retain
{
    return sharedParser;
}

- (USPostFeedParserItem*) parsePostWithPageHref: (NSString*) pageHref
                                   withPostHref: (NSString*) postHref
                                 withAuthorHref: (NSString*) authorHref
{
#ifdef UCSMTH_CONFIG_USE_PASE_PAGING_USE_PARSEKIT
    NSNumber* totalPage = [self.boardPageParser parse: pageHref];
#else
    NSNumber* totalPage = nil;
    PKTokenizer* t = [PKTokenizer tokenizerWithString: pageHref];
    PKToken* eof = [PKToken EOFToken];
    PKToken* tok = nil;
    NSUInteger current_number_index = 0;
    while ((tok = [t nextToken]) != eof) {
        if ([tok isNumber]) {
            current_number_index ++;
        }
        if (current_number_index == 2) {
            totalPage = [NSNumber numberWithInt: (int)floor(tok.floatValue)];
        }
    }
#endif
    NSString* postId = [self.feedIdParser parse: postHref];
    NSString* authorId = [self.feedAuthorIdParser parse: authorHref];
    
    USPostFeedParserItem* item = nil;
    if (totalPage && postId && authorId) {
        item = [[[USPostFeedParserItem alloc] init] autorelease];
        item.postId = postId;
        item.postAuthor = authorId;
        item.totalPage = [totalPage intValue];
    }
    
    return item;
}
@end