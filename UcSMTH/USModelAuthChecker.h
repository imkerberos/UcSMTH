//
//  USModelAuthCheck.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DDXMLDocument;

@protocol USModelAuthChecker <NSObject>
- (id<USModelAuthChecker>) checker;
- (NSError*) authCheckHtmlDocument: (DDXMLDocument*) htmlDocument;
@end

@interface USModelAuthChecker : NSObject <USModelAuthChecker>
@property (nonatomic, retain) id<USModelAuthChecker> checker;
- (id<USModelAuthChecker>) checker;
- (NSError*) authCheckHtmlDocument: (DDXMLDocument*) htmlDocument;
@end