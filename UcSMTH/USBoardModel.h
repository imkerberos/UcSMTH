//
//  USBoardModel.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USURLRequestDelegate.h"
#import "USHtmlRequestModel.h"

@interface USBoardModel : USHtmlRequestModel <USURLRequestDelegate>
@property (nonatomic, readonly, retain) NSMutableArray* topFeeds;
@property (nonatomic, readonly, retain) NSMutableArray* postFeeds;
@property (nonatomic, assign) NSUInteger viewMode;
@property (nonatomic, readonly) NSUInteger indexOfPage;
@property (nonatomic, readonly) NSUInteger totalOfPage;

+ (id) modelWithBoardId: (NSString*) aBoardId startIndexOfPage: (NSUInteger) aStartIndexOfPage;
+ (id) modelWithBoardId: (NSString*) aBoardId startIndexOfPage: (NSUInteger) aStartIndexOfPage viewMode: (NSUInteger) aViewMode;
@end
