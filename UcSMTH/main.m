//
//  main.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 AdView. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "USAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([USAppDelegate class]));
    }
}
