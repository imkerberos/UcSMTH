//
//  USBoardFeedParser.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface USBoardFeedParser : NSObject
+ (id) sharedParser;
- (id) parseBoardHref: (NSString*) href;
@end
