//
//  USArticleWebModel.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-22.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USURLRequestDelegate.h"
#import "USHtmlRequestModel.h"

@interface USArticleWebModel : USHtmlRequestModel <USURLRequestDelegate>
@property (nonatomic, readonly, copy) NSString* articleSubject;
@property (nonatomic, readonly, copy) NSString* boardId;
@property (nonatomic, readonly, copy) NSString* articleId;
@property (nonatomic, readonly, assign) NSUInteger viewMode;
@property (nonatomic, readonly, retain) NSArray* articles;

@property (nonatomic, readonly, assign) NSUInteger indexOfPage;
@property (nonatomic, readonly, assign) NSUInteger totalOfPage;

+ (id) modelWithBoardId: (NSString*) aBoardId
              articleId: (NSString*) aArticleId;

+ (id) modelWithBoardId: (NSString*) aBoardId
              articleId: (NSString*) aArticleId
               viewMode: (NSUInteger) aViewMode;

+ (id) modelWithBoardId: (NSString*) aBoardId
              articleId: (NSString*) aArticleId
       startIndexOfPage: (NSUInteger) aStartIndexOfPage
               viewMode: (NSUInteger) aViewMode;
@end
