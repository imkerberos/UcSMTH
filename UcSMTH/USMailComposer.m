//
// Created by kerberos on 12-8-27.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "USAjaxRequest.h"
#import "USMailComposer.h"
#import "SBJSON.h"
#import "NSString+SBJSON.h"

@interface USMailComposer () <ASIHTTPRequestDelegate>
@property (nonatomic, assign) USMailComposerMode mode;
@property (nonatomic, retain) USAjaxRequest* httpRequest;
@end

@implementation USMailComposer
{

@private
    NSString* _mailId;
    NSString* _boxId;
    USMailComposerMode _mode;
    USAjaxRequest* _httpRequest;
}
@synthesize mailId = _mailId;
@synthesize boxId = _boxId;
@synthesize mode = _mode;
@synthesize httpRequest = _httpRequest;


+ (id) composerWithMode: (USMailComposerMode) mode
{
    USMailComposer* vc = [[USMailComposer alloc] initWithMode: mode];
    return [vc autorelease];
}

- (id) initWithMode: (USMailComposerMode) mode
{
    NFMessageComposerHeaderFieldMask mask = NFMessageComposerHeaderFieldToMask |
        NFMessageComposerHeaderFieldSubjectMask;
    self = [super initWithFiledMask: mask];
    if (self) {
        _mode = mode;
    }
    return self;
}
- (void) dealloc
{
    [_mailId release], _mailId = nil;
    [_boxId release], _boxId = nil;
    [_httpRequest clearDelegatesAndCancel];
    [_httpRequest setDelegate: nil];
    [_httpRequest release], _httpRequest = nil;
    [super dealloc];
}

- (NSString*) title
{
    NSString* title = NSLocalizedString(@"New Mail", nil);
    switch (_mode) {
    case USMailComposerModeNew:
        title = NSLocalizedString(@"New Mail", nil);
        break;
    case USMailComposerModeReply:
        title = NSLocalizedString(@"Reply Mail", nil);
        break;
    }

    return title;
}

- (void) startSend
{
    [super startSend];
    [self postMail];
}

- (void) postMail
{

    NSString* url_format = @"http://www.newsmth.net/nForum/mail/%@/ajax_send.json";
    NSString* request_url = [NSString stringWithFormat: url_format, _boxId];
    USAjaxRequest* ajax_request = [USAjaxRequest requestWithURLString: request_url];
    [self.httpRequest clearDelegatesAndCancel];
    self.httpRequest = ajax_request;
    ajax_request.delegate = self;
    [ajax_request addPostValue: @"on" forKey: @"backup"];
    if (UcBrew_IsNotEmptyString (_mailId)) {
        [ajax_request addPostValue: _mailId forKey: @"num"];
    } else{
        [ajax_request addPostValue: @"" forKey: @"num"];
    }

    NSString* use_signature = @"0";
    if ([[[UcSMTHEngine sharedEngine] userDefaults] boolForKey: @"postsignature"]) {
        use_signature = @"1";
    }
    [_httpRequest addPostValue: use_signature forKey: @"signature"];

    NSMutableString* post_content = nil;
    post_content = [NSMutableString stringWithString: self.body];
    [post_content appendFormat: @"\n\n%@", NSLocalizedString(@"Posted by UcSMTH for iPhone.", nil)];
    [_httpRequest addPostValue: self.to forKey: @"id"];
    [_httpRequest addPostValue: post_content forKey: @"content"];
    [_httpRequest addPostValue: self.subject forKey: @"title"];

    [_httpRequest startAsynchronous];
}

- (void) requestFailed: (ASIHTTPRequest*) request
{
    NSError* error = request.error;

    NSDictionary* user_info = [NSDictionary dictionaryWithObject: [error localizedDescription] forKey:
        NSLocalizedFailureReasonErrorKey];
    NSError* e = [NSError errorWithDomain: @"ucsmth" code: 200
                                 userInfo: user_info];
    [self didFailWithError: e];
}

- (void) requestFinished: (ASIHTTPRequest*) request
{
    BOOL success = NO;
    NSString* s = [request responseString];
    UCBREW_LOG_OBJECT(request.url);
    UCBREW_LOG_OBJECT(request.requestHeaders);
    UCBREW_LOG_OBJECT(request.responseHeaders);
    UCBREW_LOG_OBJECT(s);
    id result = [s JSONValue];
    if ( result ) {
        UCBREW_LOG_OBJECT([result objectForKey:@"ajax_st"]);
        success = [[result objectForKey: @"ajax_st"] intValue];
    }

    if (success) {
        /*
            success
             */
        [self didFinishSend];
    } else {
        NSError* error_title = nil;
        if (result) {
            error_title = [result objectForKey: @"ajax_msg"];
        } else {
            error_title = @"Error";
        }

        NSDictionary* user_info = [NSDictionary dictionaryWithObject: error_title forKey:
            NSLocalizedFailureReasonErrorKey];
        NSError* e = [NSError errorWithDomain: @"ucsmth" code: 200
                                     userInfo: user_info];
        [self didFailWithError: e];
    }

}
@end