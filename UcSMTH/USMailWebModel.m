//
//  USMailWebModel.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcSMTHConfig.h"
#import "UcBrewKit.h"
#import "USURLRequest.h"
#import "USURLHtmlResponse.h"
#import "USURLRequestDelegate.h"
#import "USMailWebModel.h"

@interface USMailWebModel () <USURLRequestDelegate>
@property (nonatomic, readwrite, copy) NSString* mailSubject;
@property (nonatomic, readwrite, copy) NSString* boxId;
@property (nonatomic, readwrite, copy) NSString* mailId;
@property (nonatomic, readwrite, copy) NSString* mailDate;
@property (nonatomic, readwrite, copy) NSString* mailAuthor;
@property (nonatomic, readwrite, copy) NSString* mailBody;
@end

@implementation USMailWebModel
@synthesize mailSubject;
@synthesize mailId;
@synthesize boxId;
@synthesize mailDate;
@synthesize mailBody;
@synthesize mailAuthor;

+ (id) modelWithBoxId:(NSString *)aBoxId mailId:(NSString *)aMailId
{
    USMailWebModel* m = [[USMailWebModel alloc] initWithBoxId: aBoxId mailId: aMailId];
    return [m autorelease];
}

- (id) initWithBoxId: (NSString*) aBoxId mailId: (NSString*) aMailId
{
    self = [super init];
    if (self) {
        self.boxId = aBoxId;
        self.mailId = aMailId;
    }
    
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE (mailSubject);
    UCBREW_RELEASE (mailId);
    UCBREW_RELEASE (mailBody);
    UCBREW_RELEASE (mailDate);
    UCBREW_RELEASE (mailAuthor);
    UCBREW_RELEASE (boxId);
    [super dealloc];
}

- (void) load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (more) {
    }
    NSString* post_url = nil;
        post_url = [NSString stringWithFormat: @"http://m.newsmth.net/mail/%@/%@",
                    self.boxId, self.mailId];
    //NSString* post_url = [NSString stringWithFormat: @"http://m.newsmth.net/article/%@/%@?p=%d",
    //                      self.boardId, self.articleId, self.indexOfPage];
    //post_url = @"http://m.newsmth.net/article/Diablo/70525";
    UCBREW_LOG_OBJECT(post_url);
    USURLRequest* request = [USURLRequest requestWithURL: post_url
                                                delegate: self];
    request.cachePolicy = TTURLRequestCachePolicyNetwork;
    USURLHtmlResponse * resp = [USURLHtmlResponse response];
    request.response = resp;
    [request send];
}

- (void) requestDidStartLoad:(TTURLRequest *)request
{
    [super requestDidStartLoad: request];
}

- (void) requestDidFinishLoad:(TTURLRequest *)request
{
    USURLHtmlResponse* resp = UCBREW_DYNAMIC_CAST(request.response, USURLHtmlResponse);
    if (resp) {
        [self processHtmlDocument: resp.htmlDocument];
    }
    [super requestDidFinishLoad: request];
    //[self didFinishLoad];
}

- (void) processHtmlDocument: (DDXMLDocument*) doc
{
    NSError* error = nil;
    static NSString* article_nodes_xpath = @"//div[@id='m_main']/ul[@class='list sec']/li[2]";
    static NSString* mail_subject_xpath = @"//div[@id='m_main']/ul[@class='list sec']/li[@class='f'][1]";
    UCBREW_LOG_OBJECT(doc);
    NSArray* nodes = nil;
    NSString* mail_subject = nil;
    @try {
        NSArray* a = [doc nodesForXPath: mail_subject_xpath error: &error];
        UCBREW_LOG_OBJECT([error localizedDescription]);
        UCBREW_LOG_OBJECT( a );
        mail_subject = [[[doc nodesForXPath: mail_subject_xpath error: nil] objectAtIndex: 0] stringValue];
        NSRange r = NSMakeRange(0, 3);
        if (mail_subject && [mail_subject length] < 3) {
            r = NSMakeRange (0, [mail_subject length]);
        }
        if ([[mail_subject substringWithRange: r] isEqualToString: @"标题:"]) {
            mail_subject = [mail_subject stringByReplacingCharactersInRange:r withString: @""];
        }
        
    }
    @catch (NSException *exception) {
    }
    
    nodes = [doc nodesForXPath: article_nodes_xpath error: &error];
    if (error) {
        UCBREW_LOG(@"Error: %@\n\n", [error localizedDescription]);
    }
    @try {
        DDXMLElement* e = [nodes objectAtIndex: 0];
        NSString* author_id = nil;
        NSString* date = nil;
        NSString* date_xpath = nil;
        NSString* author_xpath = nil;
        author_xpath = @"./div[@class='nav hl']/a[1]";
        date_xpath = @"./div[@class='nav hl']/a[@class='plant'][1]";

        static NSString* content_xpath = @"./div[@class='sp']";
        author_id = [[[e nodesForXPath: author_xpath error: nil] objectAtIndex: 0] stringValue];
        
        date = [[[e nodesForXPath: date_xpath error:nil] objectAtIndex: 0] stringValue];
        
        DDXMLElement* content_node = [[e nodesForXPath: content_xpath error: nil] objectAtIndex: 0];
        static NSString* inline_image_xpath = @".//img[@src]";
        NSArray* img_node_array = [content_node nodesForXPath: inline_image_xpath error: nil];
        for (DDXMLElement* img_node in img_node_array) {
            DDXMLNode* image_parent_node = [img_node parent];
            NSString* parent_tag_name = [image_parent_node name];
            if ([parent_tag_name isEqualToString: @"a"]) {
                DDXMLElement* a_node = (DDXMLElement*) image_parent_node;
                /*
                 for (NSUInteger i = 0; i < image_parent_child_count; i ++){
                 [[[image_parent_node childAtIndex: i] name] isEqualToString: @"a"];
                 [image_parent_node 
                 }
                 */
                [a_node removeAttributeForName:@"href"];
            }
#ifdef UCSMTH_CONFIG_USE_FIX_WEBKIT_RELATION_IMAGE_URL
            NSString* image_href = [[img_node attributeForName: @"src"] stringValue];
            if (!![image_href length]) {
                /*
                 * fixed domain
                 */
                NSURL* image_url = [NSURL URLWithString: image_href];
                if (![image_url.host length]) {
                    NSString* new_href = [NSString stringWithFormat: @"http://m.newsmth.net%@", image_href];
                    DDXMLNode* href_attr = [DDXMLNode attributeWithName: @"src" stringValue: new_href];
                    [img_node removeAttributeForName: @"src"];
                    [img_node addAttribute: href_attr];
                }
            }
#endif
        }
        self.mailBody = [content_node XMLString] ;
        self.mailSubject = mail_subject;
        self.mailAuthor = author_id;
        self.mailDate = date;
    }@catch (NSException* e) {
    }
}
@end