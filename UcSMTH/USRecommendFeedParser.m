//
//  USRecommendFeedParser.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USBoardFeed.h"
#import "USPostFeed.h"
#import "USPostFeedParser.h"
#import "USRecommendFeedParser.h"

static NSString *recommend_feed_grammer = @""
        @"@wordChars = '.';"
        @"@start = feedHref;"
        @"feedHref = '/' 'article' '/' boardId '/' feedId;"
        @"boardId = Word;"
        @"feedId = Number;"
        @"";

////////////////////////////////////////////////////////////////////////////////
@interface USRecommendFeedAssembly : NSObject
- (void) parser: (PKParser*) parser didMatchBoardId: (PKAssembly*) assembly;
- (void) parser: (PKParser*) parser didMatchFeedId: (PKAssembly*) assembly;
- (void) parser: (PKParser*) parser didMatchFeedHref: (PKAssembly*) assembly;
@end
////////////////////////////////////////////////////////////////////////////////
@implementation USRecommendFeedAssembly

- (void) parser:(PKParser *)parser didMatchFeedHref:(PKAssembly *)assembly
{
    UCBREW_ASSERT(assembly.target != nil);
}

- (void) parser: (PKParser*) parser didMatchBoardId: (PKAssembly*) assembly
{
    USPostFeed* feed = assembly.target;
    if (feed == nil){
        feed = [[[USPostFeed alloc] init] autorelease];
        assembly.target = feed;
    } else {
        UCBREW_ASSERT(NO);
    }
    feed.postBoardId = [[assembly pop] stringValue];
}

- (void) parser: (PKParser*) parser didMatchFeedId: (PKAssembly*) assembly
{
    USPostFeed* feed = assembly.target;
    if (feed != nil){
        feed.postId = [[assembly pop] stringValue];
    } else {
        UCBREW_ASSERT(NO);
    }
}
@end
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
@interface  USRecommendFeedParser ()
@property (nonatomic, retain) PKParser* parser;
@property (nonatomic, retain) USRecommendFeedAssembly* assembly;
@end

////////////////////////////////////////////////////////////////////////////////
static USRecommendFeedParser* sharedParser = nil;

@implementation USRecommendFeedParser
@synthesize assembly;
@synthesize parser;

+ (id) sharedParser
{
    @synchronized ([USRecommendFeedParser class]) {
        if (sharedParser == nil) {
            sharedParser = [[USRecommendFeedParser alloc] init];
        }
    }
    
    return sharedParser;
}

+ (id) allocWithZone:(NSZone *)zone
{
    @synchronized ([USRecommendFeedParser class]) {
        if (sharedParser == nil) {
            sharedParser = [super allocWithZone: zone];
        }
    }
    return sharedParser;
}

- (id) init
{
    @synchronized ([USRecommendFeedParser class]) {
        if ((self = [super init])) {
            self.assembly = [[[USRecommendFeedAssembly alloc] init] autorelease];
            self.parser = [[PKParserFactory factory]
                           parserFromGrammar: recommend_feed_grammer
                           assembler: self.assembly];
        }
    }
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE (assembly);
    PKReleaseSubparserTree (self.parser);
    UCBREW_RELEASE (parser);
    [super dealloc];
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (oneway void) release
{
    
}

- (id) retain
{
    return sharedParser;
}

- (id) parseFeedHref: (NSString*) href
{
    PKToken* t = nil;
    t = [self.parser  parse:href];
    return t;
}
@end