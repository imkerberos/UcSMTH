//
// Created by kerberos on 12-8-26.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class NFTokenField;

@interface NFToken: UIButton
@property (nonatomic, copy) NSString* title;
@property (nonatomic, retain) id userInfo;

+ (id) tokenWithTitle: (NSString*) title userInfo: (id) userInfo;

@end

@protocol NFTokenFieldDelegate <NSObject>
@optional
- (void) tokenField: (NFTokenField*) tokenField didAddToken: (NFToken*) token;
- (void) tokenField: (NFTokenField*) tokenField didRemoveToken: (NFToken*) token;
- (void) tokenField: (NFTokenField*) tokenField didSelectToken: (NFToken*) token;
- (void) tokenFieldDidSelectAccessoryButton: (NFTokenField*) tokenField ;
@end

typedef enum {
    NFTokenFieldAccessoryTypeNone = 0,
    NFTokenFeildAccessoryTypeAdd = 1
} NFTokenFieldAccessoryType;

@interface NFTokenField : UIView
@property (nonatomic, assign) id<NFTokenFieldDelegate> delegate;
@property (nonatomic, readonly) UILabel* textLabel;
@property (nonatomic, readonly) NSArray* tokens;
@property (nonatomic, retain) UIView* accessoryView;
@property (nonatomic, assign) NFTokenFieldAccessoryType accessoryType;
@property (nonatomic, readonly) CGSize contentSize;
- (id) initWithFrame: (CGRect) frame;

- (void) addTokenWithTitle: (NSString*) title object: (id) object;
- (void) removeToken: (NFToken*) token;
- (void) removeTokenByObject: (id) object;
- (void) removeAllTokens;
@end