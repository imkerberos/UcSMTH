//
//  USAppDelegate.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UcSMTHConfig.h"

@class USNavigationController;
@interface USAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *viewController;
@property (nonatomic, strong) UIViewController* navController;
@end
