//
//  USRecomModel.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USPostFeed.h"
#import "USRecommendFeedParser.h"
#import "ASIHTTPRequest.h"
#import "ASIHTTPRequestDelegate.h"
#import "ASINetworkQueue.h"
#import "USRecommendModel.h"

@interface USRecommendModel ()
@property (nonatomic, retain) ASINetworkQueue* networkQueue;
@property (nonatomic, assign) BOOL isLoaded;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, retain, readwrite) NSMutableDictionary* recommendFeeds;
@end

@implementation USRecommendModel
@synthesize networkQueue;
@synthesize recommendFeeds;
@synthesize isLoaded;
@synthesize isLoading = _isLoading;

- (void) dealloc
{
    self.networkQueue.requestDidFinishSelector = nil;
    self.networkQueue.requestDidFinishSelector = nil;
    self.networkQueue.requestDidStartSelector = nil;
    self.networkQueue.delegate = nil;
    [self.networkQueue cancelAllOperations];
    UCBREW_RELEASE(networkQueue);
    [super dealloc];
}

- (BOOL) isOutdated
{
    return NO;
}

- (BOOL) isLoading
{
    UCBREW_LOG(@"%d", _isLoading);
    return _isLoading;
}
- (void) load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (!self.isLoading) {
        self.networkQueue = [ASINetworkQueue queue];
        self.networkQueue.delegate = self;
        self.networkQueue.shouldCancelAllRequestsOnFailure = YES;
        self.networkQueue.maxConcurrentOperationCount = 1;
        self.recommendFeeds = [NSMutableDictionary dictionaryWithCapacity: 64];
        [self.networkQueue setQueueDidFinishSelector: @selector(queueFinished:)];
        [self.networkQueue setRequestDidFailSelector: @selector(requestFailed:)];
        [self.networkQueue setRequestDidFinishSelector: @selector(requestFinished:)];

        for (NSString* s in [NSArray arrayWithObjects:
                             @"topTen", @"recommend", @"1", @"2", @"3",
                             @"4", @"5", @"6", @"7", @"8", @"9",
                             nil]) {
            NSString* url_string = [NSString stringWithFormat: @"http://m.newsmth.net/hot/%@", s];
            NSURL* url = [NSURL URLWithString: url_string];
            ASIHTTPRequest* request = [ASIHTTPRequest requestWithURL: url];
            request.useCookiePersistence = NO;
            request.userInfo = [NSDictionary dictionaryWithObject: s forKey: @"tag"];
            [self.networkQueue addOperation: request];
        }

        [self.networkQueue go];

        self.isLoading = YES;
        [self didStartLoad];
    }
}

- (void) processHtmlDocument: (DDXMLDocument*) htmlDocument withTag: (NSString*) sectionId
{
    NSString* xpath = @"//div[@id='m_main']/ul[@class='slist sec']//li[a[@href]]/a";
    NSError* e = nil;
    NSArray* nodes = [htmlDocument nodesForXPath:xpath error:&e];
    NSString* postTitle = nil;
    NSString* feedHref = nil;
    for (DDXMLElement* e in nodes) {
        postTitle = [e stringValue];
        feedHref = [[e attributeForName: @"href"] stringValue];
        USPostFeed* feed = [[USRecommendFeedParser sharedParser] parseFeedHref: feedHref];
        if (feed) {
            feed.postTitle = postTitle;
            NSMutableArray* array = [self.recommendFeeds objectForKey: sectionId];
            if (array == nil) {
                array = [NSMutableArray arrayWithCapacity: 10];
                [self.recommendFeeds setObject: array forKey: sectionId];
            }
            [array addObject: feed];
        }
    }
}

- (void) queueFinished: (ASINetworkQueue *)queue
{
    self.isLoading = NO;
    self.isLoaded = YES;
    [self didFinishLoad];
}

- (void) requestFailed: (ASIHTTPRequest*) request
{
    [self didFailLoadWithError: request.error];
}

- (void) requestFinished:(ASIHTTPRequest *)request
{
    DDXMLDocument* document = [[DDXMLDocument alloc] initWithHTMLData: request.responseData
                                                              options: HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING
                                                                error: nil];
    NSString* tag = [request.userInfo objectForKey: @"tag"];
    [self processHtmlDocument: document withTag: tag];
    [document release];
}
@end