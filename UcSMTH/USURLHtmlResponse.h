//
//  USURLHtmlResponse.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UTURLHtmlResponse.h"

@interface USURLHtmlResponse : UTURLHtmlResponse
+ (id) response;
@end
