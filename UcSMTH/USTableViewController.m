//
//  USTableViewController.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USNavigationController.h"
#import "USTableViewController.h"

@interface USTableViewController ()
@property (nonatomic, retain) USNavigationController* loginViewController;
@end

@implementation USTableViewController
@synthesize loginViewController;

- (void) dealloc
{
    UCBREW_RELEASE (loginViewController);
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.loginViewController = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void) model:(id<TTModel>)model didFailLoadWithError:(NSError *)error
{
    [super model: model didFailLoadWithError: error];
    if ([error.domain isEqualToString: UCBREW_DOMAIN]) {
        UCBREW_LOG(@"%@", [error localizedDescription]);
        NSRange r1 = [error.localizedDescription rangeOfString: @"无权阅读"];
        NSRange r2 = [error.localizedDescription rangeOfString: @"未登录"];
        if (!!r1.length || !! r2.length) {
            [self presentLoginVCWithAnimated: YES];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) presentLoginVCWithAnimated:(BOOL)animated
{
    USLoginVC* vc = [USLoginVC viewControllerWithDelegate: self];
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc]
                                   autorelease];
    [self presentModalViewController: nav animated: animated];
}

- (void) loginDidFinish:(USLoginVC*)controller
{
    UCBREW_CALL_METHOD;
    [self dismissModalViewControllerAnimated: YES];
    
    /*
     * refresh model
     */
    [self invalidateModel];
    [self invalidateView];
}

- (void) loginDidCancel:(USLoginVC*)controller
{
    UCBREW_CALL_METHOD;
    [self dismissModalViewControllerAnimated: YES];
    /*
     * failed
     */
}

@end