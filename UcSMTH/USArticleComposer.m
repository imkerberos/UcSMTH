//
// Created by kerberos on 12-8-27.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <ImageIO/ImageIO.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UcBrewKit.h"
#import "SBJSON.h"
#import "UcSMTHEngine.h"
#import "ASINetworkQueue.h"
#import "USAjaxRequest.h"
#import "USArticleComposer.h"
#import "NSString+SBJSON.h"

#define kUSArticleRequestTagAttachBase  1000
#define kUSArticleRequestTagPost  2000

@interface  USArticleComposer ()
@property (nonatomic, retain) ASINetworkQueue* networkQueue;
@property (nonatomic, assign) USArticleComposerMode mode;
@property (nonatomic, copy) NSString* errorReason;
@property (nonatomic, assign) BOOL isNetworkError;
@end

@implementation USArticleComposer
{

@private
    NSString* _boardId;
    NSString* _articleId;
    ASINetworkQueue* _networkQueue;
    USArticleComposerMode _mode;
    NSString* _errorReason;
    BOOL _isNetworkError;
}
@synthesize boardId = _boardId;
@synthesize articleId = _articleId;
@synthesize networkQueue = _networkQueue;
@synthesize mode = _mode;
@synthesize errorReason = _errorReason;
@synthesize isNetworkError = _isNetworkError;


+ (id) composerWithMode: (USArticleComposerMode) mode
{
    return [[[USArticleComposer alloc] initWithMode: mode] autorelease];
}

- (id) initWithMode: (USArticleComposerMode) mode
{
    NFMessageComposerHeaderFieldMask mask = NFMessageComposerHeaderFieldNone;
    mask = NFMessageComposerHeaderFieldAttachmentMask | NFMessageComposerHeaderFieldSubjectMask;
    self = [self initWithFiledMask: mask];
    if (self) {
        self.mode = mode;
    }

    return self;
}

- (id) initWithFiledMask: (NFMessageComposerHeaderFieldMask) mask
{
    self = [super initWithFiledMask: mask];
    if (self) {
        self.maxOfAttachments = 20;
        _networkQueue = [[ASINetworkQueue queue] retain];
        [_networkQueue setMaxConcurrentOperationCount: 1];
        _networkQueue.shouldCancelAllRequestsOnFailure = YES;
        _networkQueue.delegate = self;
        _networkQueue.queueDidFinishSelector = @selector(networkQueueDidFinish);
        _networkQueue.requestDidFinishSelector = @selector(requestFinished:);
        _networkQueue.requestDidFailSelector = @selector(requestFailed:);
    }

    return self;
}

- (void) dealloc
{
    [_boardId release], _boardId = nil;
    [_articleId release], _articleId = nil;
    _networkQueue.requestDidFailSelector = nil;
    _networkQueue.requestDidFinishSelector = nil;
    _networkQueue.delegate = nil;
    [_networkQueue cancelAllOperations];
    [_networkQueue release], _networkQueue = nil;
    [_errorReason release], _errorReason = nil;
    [super dealloc];
}

- (NSString*) title {
    NSString* title = nil;
    switch (_mode) {
    case USArticleComposerModeNew:
        title = NSLocalizedString(@"New Post", nil);
        break;
    case USArticleComposerModeEdit:
        title = NSLocalizedString(@"Edit Post", nil);
        break;
    case USArticleComposerModeReply:
        title = NSLocalizedString(@"Reply Post", nil);
        break;
    }

    return title;
}

- (void) startSend
{
    [super startSend];
    [self startPost];
}

- (void) didFinishSend
{
    [super didFinishSend];
}

#pragma mark -
#pragma mark Private
- (void) startPost
{

    NSUInteger index = 0;
    for (NSDictionary* iter in self.attachments) {
        ALAsset* asset = [iter objectForKey: kNFMessageComposerAttachementAsset];
        ALAssetRepresentation* representation = asset.defaultRepresentation;
        uint8_t* buf = (Byte*) malloc (representation.size);
        NSUInteger len = [representation getBytes: buf fromOffset: 0
                                           length: representation.size error: nil];
        if (len == 0) {
            free (buf);
            break;
        }
        NSData* data = [[NSData alloc] initWithBytesNoCopy: buf length: len freeWhenDone: YES];
        NSDictionary* source_options_dict = [NSDictionary dictionaryWithObjectsAndKeys: (id) [representation UTI],
                                                                                        kCGImageSourceTypeIdentifierHint, nil];
        UCBREW_LOG_OBJECT(source_options_dict);
        CGImageSourceRef source_ref = CGImageSourceCreateWithData ((CFDataRef) data, (CFDictionaryRef) source_options_dict);

        CFDictionaryRef image_properties_dict;
        image_properties_dict = CGImageSourceCopyPropertiesAtIndex (source_ref, 0,
            NULL);
        CFNumberRef image_width = (CFNumberRef) CFDictionaryGetValue (image_properties_dict,
            kCGImagePropertyPixelWidth);
        CFNumberRef image_height = (CFNumberRef) CFDictionaryGetValue (image_properties_dict,
            kCGImagePropertyPixelHeight);
        int w = 0;
        int h = 0;
        CFNumberGetValue (image_width, kCFNumberIntType, &w);
        CFNumberGetValue (image_height, kCFNumberIntType, &h);
        CFRelease (image_properties_dict);
        CFRelease (source_ref);
        UCBREW_LOG(@"IMAGE: {%d, %d}", w, h);
        NSString* url_format = nil;
        NSString* request_url = nil;
        url_format = @"http://www.newsmth.net/nForum/att/%@/ajax_add.json?name=ucsmth_%d.%@";
        request_url = [NSString stringWithFormat: url_format, _boardId, index,
                                                  [iter objectForKey: kNFMessageComposerAttachmentMimeType]];
        USAjaxRequest* ajax_request = [USAjaxRequest requestWithURL: request_url delegate: nil];
        [ajax_request appendPostData: data];
        [data release];
        [_networkQueue addOperation: ajax_request];
        ajax_request.tag = kUSArticleRequestTagAttachBase + index;
        index ++;
    }
    /*
    Post
     */
    USAjaxRequest* ajax_quest = nil;
    static NSString* post_format = nil;

    NSString* article_id = @"0";
    switch (_mode) {
    case USArticleComposerModeNew:
        article_id = @"";
        post_format = @"http://www.newsmth.net/nForum/article/%@/ajax_post.json";
        ajax_quest = [USAjaxRequest requestWithURLString: [NSString stringWithFormat: post_format, _boardId]];
        [ajax_quest addPostValue: article_id forKey: @"id"];
        break;
    case USArticleComposerModeReply:
        article_id = _articleId;
        post_format = @"http://www.newsmth.net/nForum/article/%@/ajax_post.json";
        ajax_quest = [USAjaxRequest requestWithURLString: [NSString stringWithFormat: post_format, _boardId]];
        [ajax_quest addPostValue: article_id forKey: @"id"];
        break;
    case USArticleComposerModeEdit:
        post_format = @"http://www.newsmth.net/nForum/article/%@/ajax_edit/%@.json";
        ajax_quest = [USAjaxRequest requestWithURLString: [NSString stringWithFormat: post_format, _boardId, _articleId]];
        [ajax_quest addPostValue: article_id forKey: @"id"];
        break;
    }
    NSMutableString* post_content = nil;
    post_content = [NSMutableString stringWithString: self.body];
    for (int i = 0; i < self.attachments.count; i++) {
        [post_content appendFormat: @"\n[upload=%d][/upload]", i + 1];
    }
    [post_content appendFormat: @"\n\n%@", NSLocalizedString(@"Posted by UcSMTH for iPhone.", nil)];
    [ajax_quest addPostValue: post_content forKey: @"content"];
    [ajax_quest addPostValue: self.subject forKey: @"subject"];

    NSString* use_signature = @"0";
    if ([[[UcSMTHEngine sharedEngine] userDefaults] boolForKey: @"postsignature"]) {
        use_signature = @"1";
    }
    /*
     * Signature option from engine.
     */
    [ajax_quest addPostValue: use_signature forKey: @"signature"];
    [ajax_quest addPostValue: @"0" forKey: @"email"];
    [ajax_quest addPostValue: @"0" forKey: @"tex"];
    ajax_quest.tag = kUSArticleRequestTagPost;
    [_networkQueue addOperation: ajax_quest];
    _isNetworkError = NO;
    [_networkQueue go];
}

#pragma mark -
#pragma mark ASIHttpRequest

- (void) requestFinished: (ASIHTTPRequest*) request
{
    NSString* s = [request responseString];
    UCBREW_LOG_OBJECT(s);
    if (request.tag < kUSArticleRequestTagPost) {
        UCBREW_LOG(@"attach request finished: %d", request.tag);
    } else if (request.tag == kUSArticleRequestTagPost) {
    }

    BOOL success = NO;
    id result = [s JSONValue];
    if ( result ) {
        UCBREW_LOG_OBJECT([result objectForKey:@"ajax_st"]);
        success = [[result objectForKey: @"ajax_st"] intValue];
    }

    if (success) {
        /*
            success
             */
    } else {
        NSError* error_title = nil;
        if (result) {
            error_title = [result objectForKey: @"ajax_msg"];
        } else {
            error_title = @"Error";
        }
        self.errorReason = error_title;
        [_networkQueue cancelAllOperations];
        _isNetworkError = YES;
    }
}

- (void) requestFailed: (ASIHTTPRequest*) request
{
    if (request.error.code == 4) {

    } else {
        [self didFailWithError: request.error];
    }
    _isNetworkError = YES;
}

- (void) networkQueueDidFinish
{
    UCBREW_CALL_METHOD;
    if (_isNetworkError) {
        /* net error */
        NSDictionary* user_info = [NSDictionary dictionaryWithObject: _errorReason forKey:
            NSLocalizedFailureReasonErrorKey];
        NSError* e = [NSError errorWithDomain: @"ucsmth" code: 200
                                     userInfo: user_info];
        [self didFailWithError: e];
    } else {
        [self didFinishSend];
    }
    self.errorReason = nil;
    _isNetworkError = NO;
}

- (void) networkQueuDidCancel
{

}
@end