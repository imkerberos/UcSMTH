//
//  USTableActivityCell.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USTableActivityItem.h"
#import "USTableActivityItemCell.h"

@implementation USTableActivityItemCell
@synthesize activityIndicatorView;

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier: reuseIdentifier];
    if (self) {
    }
    
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE(activityIndicatorView);
    [super dealloc];
}

- (void) setItem:(USTableActivityItem*)item
{
    [super setItem: item];
    if (item.text) {
        if (item.text) {
            self.textLabel.text = item.text;
        } else {
            self.textLabel.text = NSLocalizedString(@"Loading ...", nil);
        }
        self.activityIndicatorView = item.activityIndicatorView;
        [self.contentView addSubview: self.activityIndicatorView];
        self.activityIndicatorView.hidesWhenStopped = YES;
    }
}

- (void) layoutSubviews
{
    [super layoutSubviews];    [self.textLabel sizeToFit];
    CGRect r = self.contentView.bounds;
    r.size.width -= 20;
    r.origin.x += 10;
    [self.textLabel sizeToFit];
    
    CGRect new_label_rect = CGRectMake ((r.size.width - self.textLabel.frame.size.width) / 2,
                                        r.origin.y,
                                        self.textLabel.frame.size.width,
                                        r.size.height);
    
    self.textLabel.frame = new_label_rect;
    [self.activityIndicatorView sizeToFit];
    CGRect activity_rect = CGRectMake( new_label_rect.origin.x - self.activityIndicatorView.frame.size.width - 8,
                                      (r.size.height - self.activityIndicatorView.frame.size.height )/2,
                                      self.activityIndicatorView.frame.size.width,
                                      self.activityIndicatorView.frame.size.height);
    self.activityIndicatorView.frame = activity_rect;
}
@end
