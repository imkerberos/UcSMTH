//
//  USFavorVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USBoardVC.h"
#import "USFavorDS.h"
#import "USFavorVC.h"
#import "USBoardFeed.h"

@interface USFavorVC ()

@end

@implementation USFavorVC
{
@private
    NSString* _dirId;
    NSString* _dirName;
}

@synthesize dirId = _dirId;
@synthesize dirName = _dirName;

+ (id) viewControllerWithDirName: (NSString*) dirName dirId: (NSString*) dirId
{
    USFavorVC* vc = [[USFavorVC alloc] initWithStyle: UITableViewStylePlain dirName: dirName dirId: dirId];
    return [vc autorelease];
}

+ (id) viewController
{
    USFavorVC* vc = [[USFavorVC alloc] initWithStyle: UITableViewStylePlain dirName: nil dirId: nil];
    return [vc autorelease];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithStyle: (UITableViewStyle) style dirName: (NSString*) dirName dirId: (NSString*) dirId
{
    if ((self = [super initWithStyle: style])) {
        _dirId = [dirId copy];
        _dirName = [dirName copy];
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.variableHeightRows = YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma TTTableViewController

- (NSString*) title
{
    if (_dirName) {
        return _dirName;
    }
    return NSLocalizedString (@"Favorite", nil);
}

- (void) createModel
{
    self.dataSource = [USFavorDS dataSourceWithDirName: _dirName dirId: _dirId];
}

- (id) createDelegate
{
    TTTableViewDragRefreshDelegate* d = [[[TTTableViewDragRefreshDelegate alloc] initWithController:self]
                                         autorelease];
    return d;
}

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    USBoardFeed* feed = [(TTTableItem*)object userInfo];
    if (!feed.isSection && TTIsStringWithAnyText(feed.boardId)) {
        USBoardVC* vc = [USBoardVC viewControllerWithBoardId:feed.boardId];
        [self.navigationController pushViewController: vc animated: YES];
    } else if (feed.isSection){
        USFavorVC* vc = [USFavorVC viewControllerWithDirName: feed.boardName dirId: feed.boardId];
        [self.navigationController pushViewController: vc
                                             animated: YES];
    }
}

- (void) dealloc
{
    [_dirId release], _dirId = nil;
    [_dirName release], _dirName = nil;
    [super dealloc];
}
@end