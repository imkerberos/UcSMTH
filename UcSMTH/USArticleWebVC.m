//
//  USArticleWebVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "NetworkQueue.h"
#import "CMDataStorage.h"
#import "USPageVC.h"
#import "USHTMLView.h"
#import "USLoginVC.h"
#import "USNavigationController.h"
#import "USArticleComposer.h"
#import "USArticle.h"
#import "UcSMTHEngine.h"
#import "USArticleWebModel.h"
#import "USArticleWebVC.h"
#import "USWebController.h"
#import "PullToRefreshView.h"
#import "RegexKitLite.h"
#import "USMailComposer.h"
#import "USAjaxRequest.h"
#import "NSString+SBJSON.h"

#define kComposerActionForUser  1000
#define kComposerActionForOther 2000
////////////////////////////////////////////////////////////////////////////////
/*
 * WebView can't dealloc when set delegate, use a nsobject to adapter here
 */

////////////////////////////////////////////////////////////////////////////////
@interface USWebViewDelegate : NSObject <UIWebViewDelegate>
@property (nonatomic, assign) id <UIWebViewDelegate> delegate;
@end

@implementation USWebViewDelegate
@synthesize delegate;

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (self.delegate && [self.delegate respondsToSelector: @selector(webView:shouldStartLoadWithRequest:navigationType:)]) {
        return [self.delegate webView: webView shouldStartLoadWithRequest: request navigationType: navigationType];
    }
    return YES;
}
@end

////////////////////////////////////////////////////////////////////////////////
@interface USArticleWebVC () <USLoginVCDelegate, PullToRefreshViewDelegate,
        UIActionSheetDelegate, NFMessageComposerVCDelegate, ASIHTTPRequestDelegate>
@property (nonatomic, copy) NSString* boardId;
@property (nonatomic, copy) NSString* articleId;
@property (nonatomic, assign) NSUInteger viewMode;
@property (nonatomic, retain) USHTMLView* htmlView;
@property (nonatomic, retain) UIView* overlayView;
@property (nonatomic, assign) BOOL isPaged;
@property (nonatomic, assign) NSUInteger startIndexOfPage;
@property (nonatomic, retain) USWebViewDelegate* webViewDelegate;

@property (nonatomic, retain) TTActivityLabel* loadingView;
@property (nonatomic, retain) PullToRefreshView* pullView;
@property (nonatomic, assign) NSUInteger actionArticleIndex;
@property (nonatomic, retain) USAjaxRequest* deleteRequest;
@end

@implementation USArticleWebVC
{
@private
    PullToRefreshView* _pullView;
    NSUInteger _actionArticleIndex;
    USAjaxRequest* _deleteRequest;
}

@synthesize boardId;
@synthesize articleId;
@synthesize viewMode;
@synthesize overlayView;
@synthesize isPaged;
@synthesize startIndexOfPage;
@synthesize htmlView;
@synthesize loadingView;
@synthesize webViewDelegate;
@synthesize pullView = _pullView;
@synthesize actionArticleIndex = _actionArticleIndex;
@synthesize deleteRequest = _deleteRequest;


+ (id) viewControllerWithBoardId: (NSString*) aBoardId articleId: (NSString*) aArticleId viewMode: (NSUInteger) aViewMode
{
    USArticleWebVC* vc = [[USArticleWebVC alloc] initWithBoardId: aBoardId articleId: aArticleId viewMode: aViewMode];
    return [vc autorelease];
}

+ (id) viewControllerWithBoardId:(NSString *)aBoardId articleId:(NSString *) aArticleId
{
    USArticleWebVC* vc = [[USArticleWebVC alloc] initWithBoardId: aBoardId articleId: aArticleId];
    return [vc autorelease];
}

- (NSString*)createWebPath:(NSString*) path {
    path = [[NSURL fileURLWithPath:path isDirectory:NO] absoluteString];
    path = [path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    return path;
}

- (id) initWithBoardId: (NSString*) aBoardId articleId: (NSString*) aArticleId
{
    self = [self initWithBoardId: aBoardId articleId: aArticleId viewMode: 2];
    return self;
}

- (id) initWithBoardId: (NSString*) aBoardId articleId: (NSString*) aArticleId viewMode: (NSUInteger) aViewMode
{
    self = [super init];
    if (self) {
        self.boardId = aBoardId;
        self.articleId = aArticleId;
        self.viewMode = aViewMode;
        self.startIndexOfPage = 1;
        self.actionArticleIndex = -1;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.boardId = nil;
        self.articleId = nil;
        self.viewMode = 2;
        self.startIndexOfPage = 1;
    }
    return self;
}

- (void) loadView
{
    [super loadView];
    self.htmlView = [[[USHTMLView alloc] initWithFrame: CGRectZero] autorelease];
    self.webViewDelegate = [[[USWebViewDelegate alloc] init] autorelease];
    self.htmlView.tag = 999;
    self.htmlView.delegate = self.webViewDelegate;
    self.webViewDelegate.delegate = self;
    [self.view addSubview:htmlView];
}

- (void) setupHTMLView
{
    self.htmlView.addtionalStyle = @"<style type=\"text/css\">"
            "body{"
                "background: #eaeaea;"
            "}"
            ".article {"
                "border: 1px solid #9d9d9d;"
                "padding: 8px;"
                "margin-top: 16px;"
                "margin-bottom: 16px;"
                "-webkit-border-radius: 8px;"
                "background: #FFFFFF;"
            "}"
            ".subject {"
                "font-size: 18px;"
                "text-align: center;"
            "}"
            ".page {"
                "font-size: 16px;"
                "text-align: center;"
                "color: #333333;"
            "}"
            ".article_header {"
            "margin-bottom: 8px;"
            "}"
            ".line {"
                "margin-left: -8px;"
                "margin-right: -8px;"
                "margin-top: 4px;"
                "margin-bottom: 4px;"
                "border-top: 1px solid #9d9d9d;"
            "}"
            ".author {"
                "color: #333333;"
            "}"
            ".author a:link {"
                "text-decoration:none;"
                "font-size: 16px;"
            "}"
            ".date {"
                "float: right;"
                "color: #999999;"
            "}"
            ".content {"
                "padding-top: 8px;"
            "}"
            ".loadmore {"
                "background: #FFFFFF;"
                "border: 1px solid #9d9d9d;"
                "padding: 8px;"
                "margin-top: 20px;"
                "height: 40px;"
                "-webkit-border-radius: 8px;"
            "}"
            ".loadtext {"
                "color: #000066;"
                "font-size: 16px;"
                "text-align: center;"
            "}"
            ".loaddesc {"
                "color: #666666;"
                "font-size: 14px;"
                "text-align: center;"
            "}"
            ".paging {"
                "float: right;"
                "font-size: 18px;"
                "vertical-align:middle;"
                "text-align: center;"
            "}"
            "#loadmore {"
                "float: left;"
                "width: 85%;"
                "font-size: 18px;"
                "text-align: center;"
            "}"
            "img{"
                "vertical-align:middle;"
            "}"
            ".operation{"
                "padding-top: 8px;"
                "font-size: 16px;"
                "text-align: center;"
                "color: #000066;"
                "text-decoration: none"
            "}"
            "</style>";
    self.htmlView.fontSize = 16.0f;
}

- (void) setupPullView
{
    UIScrollView* current_scroll_view  = nil;
    for (UIView* subView in self.htmlView.subviews) {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            current_scroll_view = (UIScrollView *)subView;
            current_scroll_view.delegate = (id) self;
        }
    }

    PullToRefreshView* pull_view = [[PullToRefreshView alloc] initWithScrollView: current_scroll_view];
    pull_view.delegate = self;
    pull_view.tag = 998;
    [current_scroll_view addSubview: pull_view];
    [pull_view release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupHTMLView];
    [self setupViews];
    [self setupPullView];
    UIBarButtonItem* reply_item = nil;
    reply_item = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemReply
                                                                  target: self
                                                                  action: @selector(handleComposeAction)]
                    autorelease];
    self.navigationItem.rightBarButtonItem = reply_item;
}

- (void) handleComposeAction
{
    USArticleWebModel* m = (USArticleWebModel*) [self model];
    NSString* article_title = m.articleSubject;
    NSRange r = NSMakeRange (0, 4);
    if (article_title && [article_title length] < 4) {
        r = NSMakeRange (0, [article_title length]);
    }

    if ([[article_title substringWithRange: r] isEqualToString: @"Re: "]) {

    } else {
        article_title = [NSString stringWithFormat: @"Re: %@", m.articleSubject];
    }

    USArticleComposer* vc = [USArticleComposer composerWithMode: USArticleComposerModeReply];
    vc.boardId = self.boardId;
    vc.articleId = self.articleId;
    vc.subject = article_title;
    vc.delegate = self;
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc] autorelease];
    [self presentModalViewController: nav animated: YES];
}

- (void) messageComposerDidCancel: (NFMessageComposerVC*) composerVC
{
    [self dismissModalViewControllerAnimated: YES];
}

- (void) messageComposerDidFinish: (NFMessageComposerVC*) composerVC
{
    [self dismissModalViewControllerAnimated: YES];
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    if ([composerVC isMemberOfClass: [USArticleComposer class]]
        && [[engine userDefaults] boolForKey:  @"postrefresh"]) {
        USArticleWebModel* m = (USArticleWebModel*) [self model];
        self.startIndexOfPage = m.totalOfPage;
        [self invalidateModel];
        [self invalidateView];
    } else if ([composerVC isMemberOfClass: [USMailComposer class]]) {
        /*
         * do nothing
         */
    }
}

- (void) setupViews
{
    CGRect r = self.view.bounds;
    r.size.height -= 0;
    self.htmlView.frame = r;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.webViewDelegate.delegate = nil;
    self.htmlView.delegate = nil;
    UCBREW_RELEASE (htmlView);
    UCBREW_RELEASE (loadingView);
    UCBREW_RELEASE (overlayView);
}

- (void) dealloc
{
    UCBREW_CALL_METHOD;
    UIScrollView* current_scroll_view = nil;
    for (UIView* subView in self.htmlView.subviews) {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            current_scroll_view = (UIScrollView *)subView;
            current_scroll_view.delegate = nil;
        }
    }
    self.htmlView.delegate = nil;
    UCBREW_RELEASE (htmlView);
    UCBREW_OBJ_RETAINCOUNT (htmlView);
    self.webViewDelegate.delegate = nil;
    UCBREW_RELEASE(webViewDelegate);
    UCBREW_RELEASE(loadingView);
    UCBREW_RELEASE(overlayView);
    UCBREW_LOG_OBJECT(self);
    [_pullView release], _pullView = nil;
    [_deleteRequest clearDelegatesAndCancel];
    [_deleteRequest release], _deleteRequest = nil;
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString*) title
{
    if (self.viewMode == 0) {
        return NSLocalizedString (@"Single View", nil);
    } else if (self.viewMode == 2) {
        return NSLocalizedString (@"Thread View", nil);
    } else {
        return NSLocalizedString (@"Unsupported View", nil);
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    UCBREW_CALL_METHOD;
}

- (void) createModel
{
    self.model = [USArticleWebModel modelWithBoardId: self.boardId
                                           articleId: self.articleId
                                    startIndexOfPage: (NSUInteger) self.startIndexOfPage
                                            viewMode: self.viewMode];
}

- (void) modelDidFinishLoad:(id<TTModel>)model
{
    UCBREW_CALL_METHOD;
    USArticleWebModel* articleModel = nil;
    articleModel = UCBREW_DYNAMIC_CAST(model, USArticleWebModel);
    NSMutableString* articleHTMLString = [NSMutableString stringWithCapacity: 1024];
        if (!![articleModel.articleSubject length]) {
        [articleHTMLString appendFormat: @"<div class=\"subject\">%@</div>", articleModel.articleSubject];
        if (articleModel.totalOfPage > 0) {
            [articleHTMLString appendFormat: @"<div class=\"page\">[%d/%d]</div>", articleModel.indexOfPage, articleModel.totalOfPage];
        }
    }
#if 0
    if (articleModel.indexOfPage < articleModel.totalOfPage) {
#else
    if (articleModel.totalOfPage > 1) {
#endif
        NSInteger next_page = articleModel.indexOfPage + 1;
        if (next_page > articleModel.totalOfPage) {
            next_page = articleModel.totalOfPage;
        }
        NSString* loadmore = [NSString stringWithFormat: NSLocalizedString (@"Load Page %d of TotalPage: %d", nil),
                              next_page, articleModel.totalOfPage];
        NSString* loadmore_img_path = [[UcBrew bundlePath] stringByAppendingPathComponent: @"loadmore.png"];
        UCBREW_LOG_OBJECT(loadmore_img_path);
        [articleHTMLString appendFormat: @""
                     "<div class=\"loadmore\"><div id=\"loadmore\" onClick=\"window.location = "
                     "'ucsmth://loadmore';\"><span class=\"loadtext\">%@</span><br>"
                                                 "<span class=\"loaddesc\">%@</span></div>"
                                                 "<span class=\"paging\" "
                                                 "onClick=\"window"
                                                 ".location = "
                                                 "'ucsmth://paging'\"><img src=\"file://%@\"></span></div>",
                           NSLocalizedString(@"load more", nil), loadmore, loadmore_img_path];
    }
        NSUInteger article_id = 0;
    for (USArticle* article in articleModel.articles) {
        [articleHTMLString appendString: @"<div class=\"article\" id=\""];
        [articleHTMLString appendFormat: @"%d", article_id];
        [articleHTMLString appendString: @"\">"];
        [articleHTMLString appendString: @"<div class=\"article_header\">"];
        [articleHTMLString appendString: @"<span class=\"author\">"];
#if 0
        [articleHTMLString appendFormat: @"<a href=\"ucsmth://author/%@\">%@</a>", article.authorId, article.authorId];
#else
        [articleHTMLString appendFormat: @"%@", article.authorId, article.authorId];
#endif
        [articleHTMLString appendString: @"</span>"];
        [articleHTMLString appendString: @"<span class=\"date\">"];
        [articleHTMLString appendString: article.date];
        [articleHTMLString appendString: @"</span>"];
        [articleHTMLString appendString: @"</div>"];
        [articleHTMLString appendString: @"<div class=\"line\"></div>"];
        [articleHTMLString appendString: @"<div class=\"content\">"];
        [articleHTMLString appendString: article.contentHTML];
        [articleHTMLString appendString: @"</div>"];
        [articleHTMLString appendString: @"<div class=\"line\"></div>"];
        [articleHTMLString appendString: @"<div class=\"operation\" onClick=\"window.location = "
                "'ucsmth://operation/"];
        [articleHTMLString appendFormat: @"%d", article_id];
        [articleHTMLString appendString: @"';\"><span>更多操作</span></div>"];
        [articleHTMLString appendString: @"</div>"];
        article_id ++;
    }
#if 0
    if (articleModel.indexOfPage < articleModel.totalOfPage) {
#else
    if (articleModel.totalOfPage > 1) {
#endif
        NSInteger next_page = articleModel.indexOfPage + 1;
        if (next_page > articleModel.totalOfPage) {
            next_page = articleModel.totalOfPage;
        }
        NSString* loadmore = [NSString stringWithFormat: NSLocalizedString (@"Load Page %d of TotalPage: %d", nil),
                              next_page, articleModel.totalOfPage];
        NSString* loadmore_img_path = [[UcBrew bundlePath] stringByAppendingPathComponent: @"loadmore.png"];
        UCBREW_LOG_OBJECT(loadmore_img_path);
        [articleHTMLString appendFormat: @""
                     "<div class=\"loadmore\"><div id=\"loadmore\" onClick=\"window.location = "
                     "'ucsmth://loadmore';\"><span class=\"loadtext\">%@</span><br>"
                                                 "<span class=\"loaddesc\">%@</span></div>"
                                                 "<span class=\"paging\" "
                                                 "onClick=\"window"
                                                 ".location = "
                                                 "'ucsmth://paging'\"><img src=\"file://%@\"></span></div>",
                           NSLocalizedString(@"load more", nil), loadmore, loadmore_img_path];
    }
    [self.htmlView loadHTMLBody: articleHTMLString baseUrl: @"http://m.newsmth.net"];
    [self invalidateView];
    [(PullToRefreshView *)[self.htmlView viewWithTag:998] finishedLoading];
}

- (void) showEmpty:(BOOL)show
{
    UCBREW_CALL_METHOD;
}

- (CGRect)rectForOverlayView {
    return [self.htmlView frame];
}

- (void)addToOverlayView:(UIView*)view {
    if (!overlayView) {
        CGRect frame = [self rectForOverlayView];
        overlayView = [[UIView alloc] initWithFrame:frame];
        overlayView.autoresizesSubviews = YES;
        overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }

    NSInteger htmlViewIndex = [htmlView.superview.subviews indexOfObject:htmlView];
    if (htmlViewIndex != NSNotFound) {
        [htmlView.superview addSubview:overlayView];
    }

    view.frame = overlayView.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [overlayView addSubview:view];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)resetOverlayView {
    if (overlayView && !overlayView.subviews.count) {
        [overlayView removeFromSuperview];
        //UCBREW_RELEASE (overlayView);
    }
}

- (void) showLoading:(BOOL)show
{
    UCBREW_CALL_METHOD;
    if (show) {
        if (!self.model.isLoaded || ![self canShowModel]) {
            NSString* title = NSLocalizedString (@"Loading articles...", nil);
            if (title.length) {
                TTActivityLabel* label = [[[TTActivityLabel alloc] initWithStyle:TTActivityLabelStyleWhiteBox]
                 autorelease];
                label.text = title;
                label.backgroundColor = self.htmlView.backgroundColor;
                self.loadingView = label;
                [self addToOverlayView: self.loadingView];
            }
        }
    } else {
        [self.loadingView removeFromSuperview];
        [self resetOverlayView];
        self.loadingView = nil;
    }
}

- (void) presentLoginVCWithAnimated:(BOOL)animated
{
    USLoginVC* vc = [USLoginVC viewControllerWithDelegate: self];
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc]
                                   autorelease];
    [self presentModalViewController: nav animated: animated];
}

- (void) popupActionSheet
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    UIActionSheet* action_sheet = nil;
    NSString* user_id = [engine userId];

    USArticleWebModel* m = (USArticleWebModel*) [self model];
    USArticle* article = [m.articles objectAtIndex: self.actionArticleIndex];
    NSString* selected_userid = article.authorId;

    if ([user_id isEqualToString: selected_userid]) {
        action_sheet = [[UIActionSheet alloc] initWithTitle: nil
                                                   delegate: self
                                          cancelButtonTitle: NSLocalizedString(@"cancel", nil)
                                     destructiveButtonTitle: nil
                                          otherButtonTitles: NSLocalizedString(@"edit post", nil),
                                                             NSLocalizedString(@"delete post", nil),
                                                             nil
        ];
        action_sheet.destructiveButtonIndex = 1;
        action_sheet.tag = kComposerActionForUser;
    } else {
        action_sheet = [[UIActionSheet alloc] initWithTitle: nil
                                                   delegate: self
                                          cancelButtonTitle: NSLocalizedString(@"cancel", nil)
                                     destructiveButtonTitle: NSLocalizedString(@"reply", nil)
                                          otherButtonTitles: NSLocalizedString(@"mail to author", nil), nil
        ];
        action_sheet.tag = kComposerActionForOther;
    }
    [action_sheet showInView: self.view];
    [action_sheet release];
}

- (void) modelDidStartLoad:(id<TTModel>)model
{
    [super modelDidStartLoad: model];
    [self invalidateView];
    UCBREW_CALL_METHOD;
}

- (void) model:(id<TTModel>)model didFailLoadWithError:(NSError *)error
{
    [super model:model didFailLoadWithError: error];
    if ([error.domain isEqualToString: UCBREW_DOMAIN]) {
        UCBREW_LOG(@"%@", [error localizedDescription]);
        NSRange r = [error.localizedDescription rangeOfString: @"指定的文章不存在或链接错误"];
        if (!!r.length) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message: error.localizedDescription delegate: nil cancelButtonTitle: nil otherButtonTitles: @"OK", nil];
            [alertView show];
            [alertView release];
        } else {
            [self presentLoginVCWithAnimated: YES];
        }
    }
    [self invalidateView];
    UCBREW_CALL_METHOD;
}

- (void) model:(id<TTModel>)model didUpdateObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    [super model: model didUpdateObject: object atIndexPath: indexPath];
    [self invalidateView];
    UCBREW_CALL_METHOD;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    UCBREW_CALL_METHOD;
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL* url = request.URL;
    if ([url.absoluteString isEqualToString: @"about:blank"]) {
        return YES;
    } else if ([url.scheme isEqualToString: @"ucsmth"]) {
        if ([url.host isEqualToString: @"loadmore"]) {
            self.startIndexOfPage ++;
            [self reloadArticleModel];
        } else if ([url.host isEqualToString: @"operation"]) {
            int index_value = -1;
            NSRange article_index_range = NSMakeRange (1, url.path.length - 1);

            NSScanner* scanner = [NSScanner scannerWithString: [url.path substringWithRange: article_index_range]];
            UCBREW_LOG(@"%@", @"Call Operation");
            [scanner scanInt: &index_value];
            UCBREW_LOG(@"%d", index_value);
            if (index_value >= 0) {
                self.actionArticleIndex = index_value;
                [self popupActionSheet];
            }
        } else if ([url.host isEqualToString: @"paging"]) {
            USArticleWebModel* model = (USArticleWebModel* )self.model;
            USPageVC* vc = [USPageVC viewControllerWithIndexOfPage: self.startIndexOfPage totalOfPage:model.totalOfPage
                                                          delegate: self];
            [self.navigationController pushViewController: vc
                                                 animated: YES];
        }
        return NO;
    } else if ([url.host isEqualToString: @"newsmth.net"]){
        return NO;
    } else if ([url.host isEqualToString: @"m.newsmth.net"] && !![url.query length]){
        return YES;
    } else {
        [USWebController presentSharedModelControllerWithURL: request.URL.absoluteString];
        return NO;
    }
    return NO;
}

- (void) reloadArticleModel
{
    [self invalidateModel];
    [self invalidateView];
}

- (void) controller:(USPageVC *)controller didFinishPaging:(NSUInteger)page
{
    [self.navigationController popToViewController: self animated: YES];
    if (self.startIndexOfPage != page) {
        self.startIndexOfPage = page;
        [self performSelector: @selector(reloadArticleModel) withObject: nil afterDelay: 0.0f];
    }
    //[self invalidateModel];
}

- (void) loginDidFinish:(USLoginVC*)controller
{
    UCBREW_CALL_METHOD;
    [self dismissModalViewControllerAnimated: YES];
    
    /*
     * refresh model
     */
    [self invalidateModel];
    [self invalidateView];
}

- (void) loginDidCancel:(USLoginVC*)controller
{
    UCBREW_CALL_METHOD;
    [self dismissModalViewControllerAnimated: YES];
    /*
     * failed
     */
}

- (void) pullToRefreshViewShouldRefresh: (PullToRefreshView*) view
{
    [self invalidateModel];
}

- (void) actionForReplyArticle
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    USArticleWebModel* m = (USArticleWebModel*) [self model];
    USArticle* article = [m.articles objectAtIndex: self.actionArticleIndex];
    NSString* article_id = m.articleId;
    NSString* article_title = m.articleSubject;
    NSString* article_content = nil;
    NSString* article_author = article.authorId;

    if (article.artcileId) {
        article_id = article.artcileId;
    }

    NSRange r = NSMakeRange (0, 4);
    if (article_title && [article_title length] < 4) {
        r = NSMakeRange (0, [article_title length]);
    }

    if ([[article_title substringWithRange: r] isEqualToString: @"Re: "]) {

    } else {
        article_title = [NSString stringWithFormat: @"Re: %@", m.articleSubject];
    }

    if (article.contentHTML) {
        article_content =  [engine generateReplyTextWithContent: article.contentHTML
                                                         author: article_author];
    }

    USArticleComposer* vc = [USArticleComposer composerWithMode: USArticleComposerModeReply];
    vc.delegate = self;
    vc.boardId = self.boardId;
    vc.articleId = article_id;
    vc.subject = article_title;
    vc.body = article_content;

    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc] autorelease];
    [self presentModalViewController: nav animated: YES];

}

- (void) actionForEditArticle
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    USArticleWebModel* m = (USArticleWebModel*) [self model];
    USArticle* article = [m.articles objectAtIndex: self.actionArticleIndex];
    NSString* article_id = m.articleId;
    NSString* article_title = m.articleSubject;
    NSString* article_content = nil;

    if (article.artcileId) {
        article_id = article.artcileId;
    }

    if (article.contentHTML) {
        article_content =  [engine generatePlainTextWithContent: article.contentHTML];
    }

    USArticleComposer* vc = [USArticleComposer composerWithMode: USArticleComposerModeEdit];
    vc.delegate = self;
    vc.boardId = self.boardId;
    vc.articleId = article_id;
    vc.subject = article_title;
    vc.body = article_content;

    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc] autorelease];
    [self presentModalViewController: nav animated: YES];
}

- (void) actionForMailTo
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    USArticleWebModel* m = (USArticleWebModel*) [self model];
    USArticle* article = [m.articles objectAtIndex: self.actionArticleIndex];
    NSString* article_id = m.articleId;
    NSString* article_title = m.articleSubject;
    NSString* article_content = nil;
    NSString* article_author = article.authorId;

    if (article.artcileId) {
        article_id = article.artcileId;
    }

    NSRange r = NSMakeRange (0, 4);
    if ([article_title length] < 4) {
        r = NSMakeRange (0, [article_title length]);
    }
    if ([[article_title substringWithRange: r] isEqualToString: @"Re: "]) {

    } else {
        article_title = [NSString stringWithFormat: @"Re: %@", m.articleSubject];
    }

    if (article.contentHTML) {
        article_content =  [engine generateReplyTextWithContent: article.contentHTML
                                                         author: article_author];
    }

    USMailComposer* vc = [USMailComposer composerWithMode: USMailComposerModeNew];
    vc.delegate = self;
    vc.boxId = kUSMailComopserNullbox;
    vc.to = article_author;
    vc.subject = article_title;
    vc.body = article_content;
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc] autorelease];
    [self presentModalViewController: nav animated: YES];

}

- (void) actionForDelete
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    USArticleWebModel* m = (USArticleWebModel*) [self model];
    USArticle* article = [m.articles objectAtIndex: self.actionArticleIndex];
    NSString* article_id = m.articleId;
    if (article.artcileId) {
        article_id = article.artcileId;
    }
    [_deleteRequest clearDelegatesAndCancel];
    [_deleteRequest release];
    static NSString* url_format = @"http://www.newsmth.net/nForum/article/%@/ajax_delete/%@.json";
    NSString* request_url = [NSString stringWithFormat: url_format,
        self.boardId, article_id];
    _deleteRequest = [[USAjaxRequest requestWithURLString: request_url] retain];
    _deleteRequest.requestMethod = @"POST";
    _deleteRequest.delegate = self;
    [_deleteRequest startAsynchronous];
}

- (void) actionSheet: (UIActionSheet*) actionSheet didDismissWithButtonIndex: (NSInteger) buttonIndex
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    if (actionSheet.tag == kComposerActionForOther) {
        switch (buttonIndex) {
        case 0:
            /* reply */
            [self actionForReplyArticle];
            break;
        case 1:
            [self actionForMailTo];
            break;
        case 2:
            break;
        }
    } else if (actionSheet.tag == kComposerActionForUser) {
        switch (buttonIndex) {
        case 0:
            /* edit */
            [self actionForEditArticle];
            break;
        case 1:
            /* delete */
            [self actionForDelete];
            break;
        case 2:
            break;
        }
    }
}

- (void) requestFinished: (ASIHTTPRequest*) request
{
    NSString* s = request.responseString;
    id result = [s JSONValue];
    UCBREW_LOG_OBJECT(request.url);
    UCBREW_LOG_OBJECT(request.requestHeaders);
    UCBREW_LOG_OBJECT(request.responseHeaders);
    UCBREW_LOG_OBJECT(s);
    BOOL success = NO;
    if (result) {
        success = (BOOL)[[result objectForKey: @"ajax_st"] intValue];
        if (success) {
            /*
             * signle ? return
             */
            [self invalidateModel];
            [self invalidateView];
        }
    }
}
@end