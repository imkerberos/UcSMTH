//
//  USTableLongTextItem.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USTableLongTextItemCell.h"
#import "USTableLongTextItem.h"

@implementation USTableLongTextItem

- (Class) cellClass
{
    return [USTableLongTextItemCell class];
}
@end
