//
// Created by kerberos on 12-8-23.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "USPageVC.h"
#import "USPageVC.h"


@interface USPageVC ()
@property (nonatomic, readwrite) NSUInteger indexOfPage;
@property (nonatomic, readwrite) NSUInteger totalOfPage;


@end

@implementation USPageVC
{

@private
    NSUInteger _indexOfPage;
    id <USPageVCDelegate> _delegate;
    NSUInteger _totalOfPage;
}
@synthesize indexOfPage = _indexOfPage;
@synthesize delegate = _delegate;
@synthesize totalOfPage = _totalOfPage;


+ (id) viewControllerWithIndexOfPage: (NSUInteger) index1 totalOfPage: (NSUInteger) total delegate: (id) delegate
{
    USPageVC* vc = [[USPageVC alloc]
            initWithIndexOfPage: index1 totalOfPage: total
                       delegate: delegate];
    return [vc autorelease];
}

- (id) initWithIndexOfPage: (NSUInteger) aIndexOfPage totalOfPage: (NSUInteger) aTotalOfPage delegate: (id) delegate
{
    self = [super initWithStyle: UITableViewStylePlain];
    if (self) {
        self.indexOfPage = aIndexOfPage;
        self.totalOfPage = aTotalOfPage;
        self.delegate = delegate;
    }

    return self;
}

- (NSString*) title
{
    return [NSString stringWithFormat: @"%@ [%d/%d]", NSLocalizedString(@"Paging Operation", nil), _indexOfPage,
                    _totalOfPage];
}
- (void) createModel
{
    NSArray* operation_array = [NSArray arrayWithObjects:
        [TTTableTextItem itemWithText: NSLocalizedString(@"First page", nil)],
        [TTTableTextItem itemWithText: NSLocalizedString(@"Prev Page", nil)],
        [TTTableTextItem itemWithText: NSLocalizedString(@"Next Page", nil)],
        [TTTableTextItem itemWithText: NSLocalizedString(@"Last page", nil)],
        nil];

    NSMutableArray* page_array = [NSMutableArray arrayWithCapacity: _totalOfPage];
    for (int i = 1; i <= _totalOfPage; i++) {
        if (_indexOfPage == i) {
            [page_array addObject: [TTTableTextItem itemWithText: [NSString stringWithFormat: @"%d", i]
            accessory: UITableViewCellAccessoryNone]];
        } else{
            [page_array addObject: [TTTableTextItem itemWithText: [NSString stringWithFormat: @"%d", i]
            accessory: UITableViewCellAccessoryNone]];
        }
    }


    TTSectionedDataSource* data_source = nil;
    data_source = [TTSectionedDataSource dataSourceWithItems: [NSArray arrayWithObjects: operation_array, page_array]
                                                    sections: [NSArray arrayWithObjects: NSLocalizedString (@"Operation", nil),
                       NSLocalizedString(@"Page List", nil)]];
    self.dataSource = data_source;
}

- (void) didSelectObject: (id) object atIndexPath: (NSIndexPath*) indexPath
{
    NSUInteger selected_page = 1;
    if (indexPath.section == 0) {
        switch (indexPath.row) {
        case 0:
            selected_page = 1;
            break;
        case 1:
            selected_page = _indexOfPage - 1 <= 0 ? 1 : _indexOfPage - 1;
            break;
        case 2:
            selected_page = _indexOfPage + 1 >= _totalOfPage ? _totalOfPage : _indexOfPage + 1;
            break;
        case 3:
            selected_page = _totalOfPage;
            break;
        }
    } else {
        selected_page = indexPath.row + 1;
    }

    if (self.delegate && [self.delegate respondsToSelector: @selector(controller:didFinishPaging:)]) {
        [self.delegate controller: self didFinishPaging: selected_page];
    }
}
@end