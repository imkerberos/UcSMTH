//
//  USTableGrayTextItemCell.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USTableGrayTextItemCell.h"

@implementation USTableGrayTextItemCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) setObject:(id)object
{
    if (_item != object) {
        [super setObject: object];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

@end
