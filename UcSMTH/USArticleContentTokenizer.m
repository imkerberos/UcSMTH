//
//  USArticleContentTokenizer.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USArticleContentTokenizer.h"

@implementation USArticleContentTokenizer
- (PKTokenizerState *)defaultTokenizerStateFor:(PKUniChar)c
{
    if ( c == 0x3010) {
        return symbolState;
    } else {
        return [super defaultTokenizerStateFor: c];
    }
}
@end
