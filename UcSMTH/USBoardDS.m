//
//  USBoardDS.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "USBoardModel.h"
#import "USBoardDS.h"
#import "USPostFeed.h"
#import "USTableMoreButton.h"

@interface USBoardDS ()
@property (nonatomic, retain) USBoardModel* boardModel;
@end

@implementation USBoardDS
@synthesize boardModel;

+ (id) dataSourceWithBoardId: (NSString*) aBoardId startIndexOfPage: (NSUInteger) aStartIndexOfPage viewMode:(NSUInteger)aViewMode
{
    USBoardDS* ds = [[USBoardDS alloc] initWithBoardId: aBoardId startIndexOfPage: aStartIndexOfPage viewMode: aViewMode];
    return [ds autorelease];
}

- (id) initWithBoardId: (NSString*) aBoardId startIndexOfPage: (NSUInteger) aStartIndexOfPage viewMode: (NSUInteger) aViewMode
{
    self = [super init];
    if (self) {
        self.boardModel = [USBoardModel modelWithBoardId: aBoardId startIndexOfPage: aStartIndexOfPage viewMode: aViewMode];
    }
    
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE (boardModel);
    [super dealloc];
}

- (NSString*) titleForError:(NSError *)error
{
    return [super subtitleForError: error];
}

- (NSString*) subtitleForError:(NSError *)error
{
    return [error localizedDescription];
}

- (id<TTModel>) model
{
    return self.boardModel;
}

- (void) tableViewDidLoadModel:(UITableView *)tableView
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    BOOL show_top_article = [[engine userDefaults] boolForKey: @"showtoparticle"];
    NSMutableArray* array = [NSMutableArray arrayWithCapacity: self.boardModel.postFeeds.count + 2];

    NSString* view_mode_name = [engine viewModeName: self.boardModel.viewMode];
    [array addObject: [TTTableSettingsItem itemWithText: view_mode_name
                                                caption: NSLocalizedString(@"View Mode", nil)
                                              accessory: UITableViewCellAccessoryDisclosureIndicator]];
    if (show_top_article) {
        for (USPostFeed* feed in self.boardModel.topFeeds) {
            TTTableItem* item = nil;
            item = [UTTableLongSubtitleItem itemWithText: feed.postTitle
                                                subtitle: [NSString stringWithFormat:@"[%@]    %@",
                                                           feed.postDate,
                                                           feed.postAuthorId]
                                               accessory: UITableViewCellAccessoryDisclosureIndicator];
            item.userInfo = feed.postId;
            [array addObject: item];
        }
    }
    
    for (USPostFeed* feed in self.boardModel.postFeeds) {
        TTTableItem* item = nil;
        item = [UTTableLongSubtitleItem itemWithText: feed.postTitle
                                            subtitle: [NSString stringWithFormat:@"[%@]    %@",
                                                       feed.postDate,
                                                       feed.postAuthorId]
                                           accessory: UITableViewCellAccessoryDisclosureIndicator];
        item.userInfo = feed.postId;
        [array addObject: item];
    }
    
    if (self.boardModel.indexOfPage < self.boardModel.totalOfPage) {
        NSString* loadMoreText = [NSString stringWithFormat:
                                  NSLocalizedString (@"Load page: %d of total pages: %d", nil),
                                  self.boardModel.indexOfPage + 1,
                                  self.boardModel.totalOfPage];


        TTTableMoreButton* b = nil;
        b = [USTableMoreButton itemWithText: NSLocalizedString(@"load more", nil)
                                   subtitle: loadMoreText
                                  accessory: UITableViewCellAccessoryDetailDisclosureButton
                                   ];

        /*
        [TTTableMoreButton itemWithText: NSLocalizedString(@"load more", nil)
                                                      subtitle: loadMoreText
                                                        accessory: UITableViewCellAccessoryDetailDisclosureButton
                                                           URL: nil
                                                        ];
                                                        */
        [array addObject: b];
    }
    self.items = array;
}

@end