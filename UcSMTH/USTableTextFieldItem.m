//
//  USTableTextFieldItem.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USTableTextFieldItemCell.h"
#import "USTableTextFieldItem.h"

@implementation USTableTextFieldItem
@synthesize textField;
@synthesize text;

- (NSString*) reuseIdentifier
{
    return NSStringFromClass([USTableTextFieldItemCell class]);
}

- (void) dealloc
{
    UCBREW_RELEASE(text);
    [super dealloc];
}

@end
