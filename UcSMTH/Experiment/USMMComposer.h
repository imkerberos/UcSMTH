//
// Created by kerberos on 12-7-30.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "USTableViewController.h"
#import "TTTextEditor.h"
@class USMMComposer;
@protocol USMMComposerDelegate <NSObject>
@optional
- (void) composerDidFinishCompose: (USMMComposer*) composer;
- (void) composerDIdCancelCompose: (USMMComposer*) composer;
@end

@interface USMMComposer : USTableViewController
@property (nonatomic, assign) id<USMMComposerDelegate> delegate;
+ (id) viewController;
@end