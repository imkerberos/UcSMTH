//
//  USFavoriteVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-23.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#define CK_SHORTHAND
#import "MwfTableViewController.h"

@interface USForumLVC : MwfTableViewController
+ (id) viewController;
@end
