//
//  USBoardDS.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USListDataSource.h"

@interface USBoardDS : USListDataSource
+ (id) dataSourceWithBoardId: (NSString*) aBoardId startIndexOfPage: (NSUInteger) aStartIndexOfPage viewMode: (NSUInteger) aViewMode;
@end
