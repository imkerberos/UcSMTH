//
//  USTableTextViewCell.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USTableTextViewCell.h"

@implementation USTableTextViewCell
@synthesize textView;

- (void) layoutSubviews
{
    [super layoutSubviews];
    CGRect r = self.contentView.bounds;
    UCBREW_LOG_OBJECT(NSStringFromCGRect(self.contentView.frame));
    UCBREW_LOG_OBJECT(NSStringFromCGRect(self.contentView.bounds));
    [self.textView removeFromSuperview];
    r.size.height -= 8;
    r.size.width -= 8;
    r.origin.x += 4;
    r.origin.y += 4;
    self.textView.frame = r;
    [self.contentView addSubview: self.textView];
}

- (void) dealloc
{
    UCBREW_RELEASE(textView);
    [super dealloc];
}
@end
