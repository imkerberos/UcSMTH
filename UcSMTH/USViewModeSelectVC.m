//
//  USViewModeSelectVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-7-3.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USViewModeSelectVC.h"

@interface USViewModeSelectVC ()
@property (nonatomic, assign) NSInteger mode;
@end

@implementation USViewModeSelectVC
@synthesize delegate;
@synthesize mode;

+ (id) viewControllerWithDelegate:(id<USViewModeSelectVCDelegate>)delegate
{
    return [USViewModeSelectVC viewControllerWithMode: -1 delegate: delegate];
}

+ (id) viewControllerWithMode: (NSInteger)aMode
                     delegate: (id<USViewModeSelectVCDelegate>) delegate
{
    USViewModeSelectVC* vc = [[USViewModeSelectVC alloc] initWithStyle:UITableViewStylePlain mode: aMode];
    
    vc.delegate = delegate;
    return [vc autorelease];
}

- (id) initWithStyle:(UITableViewStyle)style mode: (NSInteger) aMode
{
    self = [super initWithStyle: style];
    if (self) {
        self.mode = aMode;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self myCreatTableData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (MwfTableData*) createAndInitTableData
{
    return nil;
}

- (void) myCreatTableData
{
    MwfTableData* table_data = [MwfTableData createTableData];
    [table_data addRow: [NSNumber numberWithInt: 0]];
    //[table_data addRow: [NSNumber numberWithInt: 1]];
    [table_data addRow: [NSNumber numberWithInt: 2]];
    //[table_data addRow: [NSNumber numberWithInt: 3]];
    self.tableData = table_data;
}

- (NSString*) title
{
    return NSLocalizedString(@"View Mode", nil);
}

- $cellFor (NSNumber)
{
    static NSString* identifier = @"NumberCell";
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                       reuseIdentifier: identifier]
                autorelease];
    }
    
    return cell;
}

- $configCell (NSNumber, UITableViewCell)
{
    if (item.intValue == 0) {
        cell.textLabel.text = NSLocalizedString(@"Classic", nil);
#if 0
    } else if (item.intValue == 1) {
        cell.textLabel.text = NSLocalizedString(@"Abstract", nil);
#endif
    } else if (item.intValue == 2) {
        cell.textLabel.text = NSLocalizedString(@"Thread", nil);
    }
#if 0
    else if (item.intValue == 3) {
        cell.textLabel.text = NSLocalizedString(@"Saved", nil);
    }
#endif
    //cell.textLabel.textAlignment = UITextAlignmentRight;
    if (self.mode == item.intValue) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber* number = nil;
    MwfTableData* table_data;
    table_data = [self tableDataForTableView: tableView];
    number = [table_data objectForRowAtIndexPath: indexPath];
    if (self.delegate && [self.delegate respondsToSelector: @selector(viewModeSelectDidFinish:mode:)]) {
        [self.delegate viewModeSelectDidFinish: self mode: number.intValue];
    }
    
    [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex: 1] animated:YES];
}

- (void) handleCancelAction
{
    if (self.delegate && [self.delegate respondsToSelector: @selector(viewModeSelectDidCancel:)]){
        [self.delegate viewModeSelectDidCancel: self];
    }
    [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex: 1] animated:YES];
}
@end