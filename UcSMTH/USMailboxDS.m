//
//  USMailboxDS.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USMailFeed.h"
#import "USMailboxModel.h"
#import "USMailboxDS.h"
#import "USTableMoreButton.h"

@interface  USMailboxDS ()
@property (nonatomic, retain) USMailboxModel* mailboxModel;
@end

@implementation USMailboxDS
@synthesize mailboxModel;

+ (id) dataSourceWithBoxId:(NSString *)aBoxId startIndexOfPage:(NSUInteger)aStartIndexOfPage
{
    USMailboxDS* ds = [[USMailboxDS alloc] initWithBoxId: aBoxId startIndexOfPage: aStartIndexOfPage];
    return [ds autorelease];
}

- (id) initWithBoxId: (NSString*) aBoxId startIndexOfPage: (NSUInteger) aStartIndexOfPage
{
    self = [super init];
    if (self) {
        self.mailboxModel = [USMailboxModel modelWithBoxId: aBoxId startIndexOfPage: aStartIndexOfPage];
    }
    
    return self;
}

- (id<TTModel>) model
{
    return self.mailboxModel;
}

- (NSString*) titleForEmpty
{
    return NSLocalizedString(@"No mail in mailbox", nil);
}

- (NSString*) titleForError:(NSError *)error
{
    return [super subtitleForError: error];
}

- (NSString*) subtitleForError:(NSError *)error
{
    return [error localizedDescription];
}

- (NSString*) titleForLoading:(BOOL)reloading
{
    if (reloading) {
        return NSLocalizedString(@"Updating mail ...", nil);
    } else {
        return NSLocalizedString(@"Loading mail ...", nil);
    }
}

- (void) tableViewDidLoadModel:(UITableView *)tableView
{
    NSMutableArray* item_array = [[NSMutableArray alloc] initWithCapacity: self.mailboxModel.mailFeeds.count];
    
    for (USMailFeed* feed in self.mailboxModel.mailFeeds) {
        UTTableLongSubtitleItem * item = [UTTableLongSubtitleItem itemWithText: feed.mailSubject
                                                                      subtitle: [NSString stringWithFormat: @"[ %@ ] %@", feed.mailDate, feed.mailAuthor]];
        UCBREW_LOG_OBJECT(feed.mailId);
        item.userInfo = feed;
        [item_array addObject: item];
    }
    UCBREW_LOG(@"%d/%d", self.mailboxModel.indexOfPage, self.mailboxModel.totalOfPage);
    if (self.mailboxModel.indexOfPage < self.mailboxModel.totalOfPage) {
        NSString* loadMoreText = [NSString stringWithFormat:
                                  NSLocalizedString(@"Load page: %d of total pages: %d", nil),
                                  self.mailboxModel.indexOfPage + 1,
                                  self.mailboxModel.totalOfPage];
        TTTableMoreButton* b = [USTableMoreButton itemWithText: NSLocalizedString(@"load more", nil)
                                                      subtitle: loadMoreText
        accessory: UITableViewCellAccessoryDetailDisclosureButton];
        [item_array addObject: b];
    }
    
    self.items = item_array;
    [item_array release];
}

- (void) dealloc
{
    UCBREW_RELEASE(mailboxModel);
    [super dealloc];
}
@end
