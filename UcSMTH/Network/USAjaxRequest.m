//
// Created by kerberos on 12-8-25.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "USAjaxRequest.h"


@implementation USAjaxRequest
{

}

- (id) initWithURL: (NSURL*) newURL
{
    self = [super initWithURL: newURL];
    if (self) {
        [self addRequestHeader: @"X-Requested-With" value: @"XMLHttpRequest"];
        [self addRequestHeader: @"Host" value: newURL.host];
    }

    return self;
}

+ (id) requestWithURL: (NSString*) aURL delegate: (id) aDelegate
{
    NSURL* url = [NSURL URLWithString: aURL];
    USAjaxRequest* request = [[USAjaxRequest alloc] initWithURL: url];
    request.delegate = aDelegate;
    return [request autorelease];
}

+ (id) requestWithURLString: (NSString*) aURLString
{
    return [USAjaxRequest requestWithURL: aURLString delegate: nil];
}


@end