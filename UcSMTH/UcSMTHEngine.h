//
//  USEngine.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UCSMTH_APPSTORE_URL @"http://itunes.apple.com/us/app/ucsmth/id543183096?mt=8"
@interface UcSMTHEngine : NSObject
@property (nonatomic, readonly) NSString* cacheFilePath;
@property (nonatomic, readonly) NSUserDefaults* userDefaults;
@property (nonatomic, readonly) NSString* bundleIdentifier;
@property (nonatomic, readonly) BOOL isLogin;
+ (id) sharedEngine;
- (void) setupEngine;
- (NSString*) boardNameById: (NSString*) boardId;
- (NSString*) sectionNameById: (NSString*) sectionId;
- (NSString*) appVersion;
- (NSString*) bundleIdentifier;
- (void) setupDefaultConfig;
- (void) invalidLogin;
- (NSString*) viewModeName: (NSUInteger) mode;
- (NSString*) generateReplyTextWithContent: (NSString*) aArticleHtmlContent author: (NSString*) aArticleAuthor;
- (NSString*) generatePlainTextWithContent: (NSString*) aArticleHtmlContent;
- (NSString*) generateReplyMailWithContent: (NSString*) aMailHtmlContent author: (NSString*) aMailAuthor;
- (NSString*) userId;
- (void) userLogoout;
@end
