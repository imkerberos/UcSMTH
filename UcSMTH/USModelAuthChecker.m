//
//  USModelAuthCheck.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "DDXML.h"
#import "DDXML+HTML.h"
#import "UcBrewKit.h"
#import "USModelAuthChecker.h"

@implementation USModelAuthChecker
@synthesize checker;

- (id<USModelAuthChecker>) checker
{
    return !!checker ? checker : self;
}

- (NSError*)isNeedAuthForHtmlDocument:(DDXMLDocument *)htmlDocument
{
    return [(id<USModelAuthChecker>)checker authCheckHtmlDocument: htmlDocument];
}

- (NSError*) authCheckHtmlDocument:(DDXMLDocument *)htmlDocument
{
    NSError* error = nil;
    NSString* xpath = @"//div[@id='m_main']/div[@class='sp hl f']";
    NSArray* nodes = [htmlDocument nodesForXPath: xpath error: &error];
    if (error) {
        return nil;
    } else if (!! nodes.count){
        NSString* message = nil;
        message = [[nodes objectAtIndex: 0] stringValue];
        NSRange r = [message rangeOfString:@"成功"];
        if (!r.length) {
            NSDictionary* infoDict = [NSDictionary dictionaryWithObject: message
                                                                 forKey: NSLocalizedDescriptionKey];
            return [NSError errorWithDomain: UCBREW_DOMAIN code: 100 userInfo: infoDict];
        }
    }
    return nil;
}
@end