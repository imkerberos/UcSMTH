//
// Created by kerberos on 12-8-19.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

#import "TTPhotoSource.h"

@interface USImageDS : NSObject <TTPhotoSource>

- (id) initWithPhotos: (id) photoArray;
- (NSInteger) numOfPhotos;
- (NSInteger) maxPhotoIndex;
- (id<TTPhoto>) photoAtIndex: (NSInteger) photoIndex;
@end