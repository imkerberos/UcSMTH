//
//  USBoardFeed.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface USBoardFeed : NSObject
@property (nonatomic, copy) NSString* boardId;
@property (nonatomic, copy) NSString* boardName;
@property (nonatomic, assign) BOOL isSection;
@end
