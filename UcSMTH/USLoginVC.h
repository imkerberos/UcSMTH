//
//  USSignUpVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#define CK_SHORTHAND
#import "MwfTableViewController.h"

@class USLoginVC;

@protocol USLoginVCDelegate <NSObject>
@required
- (void) loginDidFinish: (USLoginVC*) controller;
- (void) loginDidCancel: (USLoginVC*) controller;
@end

@interface USLoginVC : MwfTableViewController
+ (id) viewControllerWithDelegate: (id) delegate;
@end
