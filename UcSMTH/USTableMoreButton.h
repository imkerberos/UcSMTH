//
// Created by kerberos on 12-8-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "TTTableMoreButton.h"

@interface USTableMoreButton : TTTableMoreButton
+ (id) itemWithText: (NSString*) text subtitle: (NSString*) subtitle accessory: (UITableViewCellAccessoryType)
        accessoryType;
+ (id) itemWithText: (NSString*) text subtitle: (NSString*) subtitle accessory: (UITableViewCellAccessoryType)
        accessoryType delegate: (id) delegate selector: (SEL) selector;
@end