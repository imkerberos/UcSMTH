//
//  USURLHtmlResponse.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USURLHtmlResponse.h"

@implementation USURLHtmlResponse
+ (id) response
{
    USURLHtmlResponse* resp = [[USURLHtmlResponse alloc] init];
    return [resp autorelease];
}
@end
