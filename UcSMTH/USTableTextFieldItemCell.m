//
//  USTableTextFieldCell.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USTableTextFieldItem.h"
#import "USTableTextFieldItemCell.h"

@implementation USTableTextFieldItemCell
@synthesize textField;

- (void) setItem:(USTableTextFieldItem*)item
{
    [super setItem: item];
    self.textField = item.textField;
    self.textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.textLabel.text = item.text;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView addSubview: item.textField];
    //item.textField.leftView = self.textLabel;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    [self.textLabel sizeToFit];
    CGRect r = self.contentView.bounds;
    r.size.width -= 20;
    r.origin.x += 10;
    [self.textLabel sizeToFit];
    CGRect new_label_rect = CGRectMake (r.origin.x, r.origin.y, self.textLabel.frame.size.width, r.size.height);
    CGRect text_field_rect = CGRectMake(r.origin.x + self.textLabel.frame.size.width + 8, r.origin.y,  r.size.width - new_label_rect.size.width - 8, r.size.height);
    self.textLabel.frame = new_label_rect;
    self.textField.frame = text_field_rect;
#if 0
    CGRect r = self.contentView.bounds;
    r.size.height -= 16;
    r.size.width -= (8 + self.textLabel.frame.size.width + 4 + self.textLabel.frame.origin.x);
    r.origin.x = self.textLabel.frame.origin.x + self.textLabel.frame.size.width + 4;
    r.origin.y += 8; 
    
    [self.textField removeFromSuperview];
    self.textField.frame = r;
    [self.contentView addSubview: self.textField];
#endif
}

- (void) dealloc
{
    UCBREW_RELEASE(textField);
    [super dealloc];
}

@end
