//
//  UcSMTHEngine.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMResultSet.h"
#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "RegexKitLite.h"

static NSString* UcSMTHCacheFile = @"UcSMTH.cache";
static UcSMTHEngine* sharedEngine = nil;

////////////////////////////////////////////////////////////////////////////////
@interface UcSMTHEngine ()
@property (nonatomic, retain) NSFileManager* fileManager;
@property (nonatomic, retain) FMDatabase* sqlite;
@end
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
@implementation UcSMTHEngine
@synthesize fileManager;
@synthesize cacheFilePath;
@synthesize userDefaults;
@synthesize sqlite;
@synthesize isLogin;

+ (id) sharedEngine
{
    @synchronized ([UcSMTHEngine class]) {
        if (sharedEngine == nil) {
            sharedEngine = [[UcSMTHEngine alloc] init];
        }
    }
    
    return sharedEngine;
}

+ (id) allocWithZone:(NSZone *)zone
{
    @synchronized ([UcSMTHEngine class]) {
        if (sharedEngine == nil) {
            sharedEngine = [super allocWithZone: zone];
        }
    }
    return sharedEngine;
}

- (id) init
{
    @synchronized ([UcSMTHEngine class]) {
        if ((self = [super init])) {
            self.fileManager = [[[NSFileManager alloc] init] autorelease];
        }
    }
    return self;
}

- (void) dealloc
{
    [super dealloc];
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (oneway void) release
{
    
}

- (id) retain
{
    return sharedEngine;
}

- (NSString*) cacheFilePath
{
    return [[UcBrew cachesPath] stringByAppendingPathComponent: UcSMTHCacheFile];
}

- (void) setupEngine
{
    [self configureApplication];
    if (self.sqlite == nil) {
        self.sqlite = [FMDatabase databaseWithPath: self.cacheFilePath];
    }
    [self.sqlite open];
}

- (NSString*) sectionNameById: (NSString*) sectionId
{
    NSString* name = nil;
    FMResultSet* rs = [self.sqlite executeQuery: @"select name from section where id = ?", sectionId];
    if ([rs next]) {
        name = [rs stringForColumn: @"name"];
    }
    return name;
}

- (NSString*) boardNameById: (NSString*) boardId
{
    NSString* name = nil;
    FMResultSet* rs = [self.sqlite executeQuery: @"select name from board where id = ?", boardId];
    if ([rs next]) {
        name = [rs stringForColumn: @"name"];
    }
    if (name == nil) {
        UCBREW_LOG(@"Can't find Board name: %@ with Id: %@", name, boardId);
    }
    return name;
}

- (BOOL) configureApplication
{
    [[TTURLRequestQueue mainQueue] setUserAgent: @"UcSMTH"];
    [[TTURLRequestQueue mainQueue] setMaxContentLength: 0];
    [[TTURLRequestQueue mainQueue] setDefaultTimeout: 30];
    [self copyFromCache];
    
    return YES;
}

- (BOOL) copyFromCache
{
    BOOL need_update = NO;
    static NSString* kCacheVersionKey = @"ucsmth_version";
    NSString* current_version = [self appVersion];
    NSString* cache_version_file = [[UcBrew cachesPath] stringByAppendingPathComponent: @"Versions"];
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithContentsOfFile: cache_version_file];
    NSString* cache_version = [dict objectForKey: kCacheVersionKey];

    if (![cache_version length]) {
        need_update = YES;
    } else {
        float current_ver_num = 0;
        float cache_ver_num = 0;
        NSScanner* scanner = [NSScanner scannerWithString: current_version];
        [scanner scanFloat: &current_ver_num];
        scanner = [NSScanner scannerWithString: cache_version];
        [scanner scanFloat: &cache_ver_num];
        if (cache_ver_num < current_ver_num) {
            need_update = YES;
        }
        UCBREW_LOG(@"cur version: %f, oldver: %f", current_ver_num, cache_ver_num);
    }

    if (need_update) {
        UCBREW_LOG(@"need update cache db");
        FMDatabase* current_db = [FMDatabase databaseWithPath: [[UcBrew bundlePath]
                                                                        stringByAppendingPathComponent: UcSMTHCacheFile]];
        FMDatabase* cache_db = [FMDatabase databaseWithPath: self.cacheFilePath];
        UCBREW_LOG_OBJECT(self.cacheFilePath);
        [current_db open];
        [cache_db open];
        if (![cache_db tableExists: @"section"]) {
            [cache_db executeUpdate: @"create table 'section' ('id' text primary key, 'name' text, "
                "'section' text)"];
        }

        if (![cache_db tableExists: @"board"]) {
            [cache_db executeUpdate: @"create table 'board' ('id' text primary key, 'name' text, 'section' text)"];
        }

        FMResultSet* rs = [current_db executeQuery: @"select id, name, section from section"];
        [cache_db beginTransaction];
        while ([rs next]) {
            NSString* section_id = [rs stringForColumn: @"id"];
            NSString* section_name = [rs stringForColumn: @"name"];
            NSString* section_section = [rs stringForColumn: @"section"];
            [cache_db executeUpdate: @"insert or replace into section (id, name, section) values (?,?,?)",
                                    section_id, section_name, section_section];
        }
        [cache_db commit];
        rs = [current_db executeQuery: @"select id, name, section from board"];
        [cache_db beginTransaction];
        while ([rs next]) {
            NSString* section_id = [rs stringForColumn: @"id"];
            NSString* section_name = [rs stringForColumn: @"name"];
            NSString* section_section = [rs stringForColumn: @"section"];
            [cache_db executeUpdate: @"insert or replace into board (id, name, section) values (?,?,?)",
                                    section_id, section_name, section_section];
        }
        [cache_db commit];
        [current_db close];
        [cache_db close];

        if (!dict) {
            dict = [NSMutableDictionary dictionary];
        }
        [dict setObject: current_version forKey: kCacheVersionKey];
        [dict writeToFile: cache_version_file atomically: YES];
    } else {
        UCBREW_LOG(@"not need update cache db");
    }

    return YES;
}

- (NSUserDefaults*) userDefaults
{
    return [NSUserDefaults standardUserDefaults];
}

- (NSString*) bundleIdentifier
{
    return [[NSBundle mainBundle] bundleIdentifier];
}

- (NSString*) appVersion
{
    return [[[NSBundle mainBundle] infoDictionary] valueForKey: @"CFBundleShortVersionString"];
}

- (void) setupDefaultConfig
{
    NSUserDefaults *defaults = [self userDefaults];
    [defaults registerDefaults:[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"UserDefaults" ofType:@"plist"]]];
}

- (void) invalidLogin
{
    NSHTTPCookieStorage* cookie = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie* c in [cookie cookiesForURL:[NSURL URLWithString:@"http://m.newsmth.net"]]) {
        [cookie deleteCookie: c];
    }
}

- (NSString*) viewModeName: (NSUInteger) mode
{
    NSString* name;
    switch (mode) {
    case -1:
        name = NSLocalizedString(@"Auto", nil);
        break;
    case 0:
        name = NSLocalizedString(@"Classic", nil);
        break;
    case 1:
        name = NSLocalizedString(@"Marked", nil);
        break;
    case 2:
        name = NSLocalizedString(@"Thread", nil);
        break;
    case 3:
        name = NSLocalizedString(@"Saved", nil);
        break;
    default:
        name = NSLocalizedString(@"Unsupported View", nil);
        break;
    }

    return name;
}


- (BOOL) isLogin
{
    NSHTTPCookieStorage* cookie = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSUInteger cookie_count = 0;
    for (NSHTTPCookie* c in [cookie cookiesForURL:[NSURL URLWithString:@"http://m.newsmth.net"]]) {
        if ([c.name isEqualToString:@"main[UTMPUSERID]"]) {
            cookie_count ++;
        }
        
        if ([c.name isEqualToString: @"main[PASSWORD]"]) {
            cookie_count ++;
        }
    }
    
    return cookie_count >= 2;
}

- (NSString*) userId
{
    NSString* userid = nil;
    NSHTTPCookieStorage* cookie = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie* c in [cookie cookiesForURL:[NSURL URLWithString:@"http://m.newsmth.net"]]) {
        if ([c.name isEqualToString:@"main[UTMPUSERID]"]) {
            userid = c.value;
            break;
        }
    }

    return userid;
}

- (void) userLogout
{
    NSHTTPCookieStorage* cookie = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie* c in [cookie cookiesForURL:[NSURL URLWithString:@"http://m.newsmth.net"]]) {
        [cookie deleteCookie: c];
    }
}


- (NSString*) generateReplyTextWithContent: (NSString*) aArticleHtmlContent author: (NSString*) aArticleAuthor
{
    NSMutableString* article_content = nil;

    DDXMLDocument* doc = [[DDXMLDocument alloc] initWithHTMLString: aArticleHtmlContent
                                                           options: HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING
                                                             error: nil];
    article_content = [NSMutableString stringWithFormat: @"\n【 在 %@ 的大作中提到: 】", aArticleAuthor];
    if (doc) {
        static NSString* tail_ip_regex = @"^FROM \\d+\\.\\d+\\.\\d+\\..*";
        static NSString* reply_refer_regex = @"【 在 .* 的大作中提到: 】";

        NSArray* text_array = [doc nodesForXPath: @"//*/text()" error: nil];
        UCBREW_LOG_OBJECT(text_array);
        for (DDXMLNode* n in text_array) {
            NSString* t = [n stringValue];
            if ([t isMatchedByRegex: reply_refer_regex]) {
                break;
            }
            if (t && ![t  isEqualToString: @"--"] && ![t isMatchedByRegex: tail_ip_regex] ) {
                [article_content appendFormat: @"\n: %@", t];
            }
        }
        UCBREW_LOG_OBJECT(article_content);
    }

    [doc release];

    return article_content;
}

- (NSString*) generatePlainTextWithContent: (NSString*) aArticleHtmlContent
{
    NSMutableString* article_content = nil;

    DDXMLDocument* doc = [[DDXMLDocument alloc] initWithHTMLString: aArticleHtmlContent
                                                           options: HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING
                                                             error: nil];
    article_content = [NSMutableString string];
    if (doc) {
        NSArray* text_array = [doc nodesForXPath: @"//*/text()" error: nil];
        UCBREW_LOG_OBJECT(text_array);
        for (DDXMLNode* n in text_array) {
            NSString* t = [n stringValue];
            [article_content appendFormat: @"\n%@", t];
        }
        UCBREW_LOG_OBJECT(article_content);
    }

    [doc release];

    return article_content;
}

- (NSString*) generateReplyMailWithContent: (NSString*) aMailHtmlContent author: (NSString*) aMailAuthor
{
    NSMutableString* article_content = nil;

    DDXMLDocument* doc = [[DDXMLDocument alloc] initWithHTMLString: aMailHtmlContent
                                                           options: HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING
                                                             error: nil];
    article_content = [NSMutableString stringWithFormat: @"\n【 在 %@ 的来信中提到: 】", aMailAuthor];
    if (doc) {
        static NSString* tail_ip_regex = @"^FROM \\d+\\.\\d+\\.\\d+\\..*";
        static NSString* reply_refer_regex = @"【 在 .* 的来信中提到: 】";

        NSArray* text_array = [doc nodesForXPath: @"//*/text()" error: nil];
        UCBREW_LOG_OBJECT(text_array);
        for (DDXMLNode* n in text_array) {
            NSString* t = [n stringValue];
            if ([t isMatchedByRegex: reply_refer_regex]) {
                break;
            }
            if (t && ![t  isEqualToString: @"--"] && ![t isMatchedByRegex: tail_ip_regex] ) {
                [article_content appendFormat: @"\n: %@", t];
            }
        }
        UCBREW_LOG_OBJECT(article_content);
    }

    [doc release];

    return article_content;
}
@end
////////////////////////////////////////////////////////////////////////////////