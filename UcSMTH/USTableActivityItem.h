//
//  USTableActivityItem.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "MwfTableItem.h"

@interface USTableActivityItem : MwfTableItem
@property (nonatomic, copy) NSString* text;
@property (nonatomic, retain) UIActivityIndicatorView* activityIndicatorView;
@end
