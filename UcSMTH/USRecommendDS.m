//
//  USRecommendDS.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USRecommendModel.h"
#import "UcSMTHEngine.h"
#import "USPostFeed.h"
#import "USRecommendModel.h"
#import "USRecommendDS.h"

@interface USRecommendDS ()
@property (nonatomic, retain) USRecommendModel* recommendModel;
@end

@implementation USRecommendDS
@synthesize recommendModel;

+ (id) dataSource
{
    USRecommendDS* ds = [[USRecommendDS alloc] init];
    return [ds autorelease];
}

- (id) init
{
    self = [super init];
    if (self) {
        self.recommendModel = [[[USRecommendModel alloc] init] autorelease];
    }
    
    return self;
}

- (id<TTModel>) model
{
    return self.recommendModel;
}

- (void) dealloc
{
    UCBREW_RELEASE (recommendModel);
    [super dealloc];
}

- (NSString*) title
{
    return NSLocalizedString(@"Recommends", nil);
}

- (NSString*) titleForLoading:(BOOL)reloading
{
    if (reloading) {
        return NSLocalizedString(@"Reloading recommends ...", nil);
    } else {
        return NSLocalizedString(@"Loading recommends ...", nil);
    }
}

- (NSString*) titleForEmpty
{
    return NSLocalizedString(@"Sorry, can't fetch recommends.", nil);
}

- (NSString*) titleForError:(NSError *)error
{
    return [error localizedDescription];
}

- (void) tableViewDidLoadModel:(UITableView *)tableView
{
    UCBREW_CALL_METHOD;
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    NSDictionary* feedsDict = self.recommendModel.recommendFeeds;
    NSUInteger sectionCounts = [[feedsDict allKeys] count];
    NSMutableArray* sections = [NSMutableArray arrayWithCapacity: sectionCounts];
    NSMutableArray* items = [NSMutableArray arrayWithCapacity: sectionCounts];
    NSArray* key_order = [NSArray arrayWithObjects:
                         @"topTen", @"recommend", @"1", @"2", @"3",
                         @"4", @"5", @"6", @"7", @"8", @"9"
                         ,
                         nil];
    
    NSArray* sorted_keys = [[self.recommendModel.recommendFeeds allKeys] sortedArrayUsingComparator: ^(id obj1, id obj2) {
        if ([key_order indexOfObject: obj1] > [key_order indexOfObject: obj2]) {
            return NSOrderedDescending;
        } else {
            return NSOrderedAscending;
        }
    }];
    
    for (NSString* sectionId in sorted_keys) {
        NSArray* a = [self.recommendModel.recommendFeeds objectForKey: sectionId];
        NSMutableArray* itemArray = [NSMutableArray arrayWithCapacity: a.count];
        for (USPostFeed* feed in a) {
            UTTableLongSubtitleItem* item = nil;
            NSString* board_name = [engine boardNameById: feed.postBoardId];
            item = [UTTableLongSubtitleItem itemWithText: feed.postTitle
                                                subtitle: [NSString stringWithFormat: @"[%@]", board_name]
                                               accessory: UITableViewCellAccessoryDisclosureIndicator
                    ];
            item.userInfo = feed;
            [itemArray addObject: item];
        }
        if (!!itemArray.count) {
            [items addObject: itemArray];
            NSString* sectionName = [[UcSMTHEngine sharedEngine] sectionNameById: sectionId];
            if (sectionName) {
                [sections addObject: sectionName];
            }
        }
    }
    
    self.items = items;
    self.sections = sections;
}
@end
